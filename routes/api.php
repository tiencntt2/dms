<?php

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Faker;
use \App\JsonResponse;
use \App\Acl;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function() {
    /* For Admin, Manager */
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/reset-password', 'AuthController@resetPassword');
    Route::post('client/reset-password', 'AuthController@clientResetPassword');
    Route::group(['middleware' => 'auth:sanctum'], function () {
        // Auth routes
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');

        Route::get('/user', function (Request $request) {
            return new UserResource($request->user());
        });

        // Api resource routes
        Route::apiResource('roles', 'RoleController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::apiResource('permissions', 'PermissionController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);

        // Custom routes
        Route::put('users/{user}', 'UserController@update');
        Route::get('users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::put('users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' .Acl::PERMISSION_PERMISSION_MANAGE);
        Route::get('roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);

        //Customer route
        Route::get('/customers', 'CustomController@index')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::post('/customer/create', 'CustomController@create')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::post('/customer/store', 'CustomController@store')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::get('/customer/edit/{id}', 'CustomController@edit')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::get('/customer/getEdit/{id}', 'CustomController@getEdit')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::post('/customer/show/{id}', 'CustomController@show')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::get('/customer/confirm/{id}', 'CustomController@confirm')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::put('/customer/update/{id}', 'CustomController@update')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::delete('/customer/delete/{id}', 'CustomController@destroy')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);
        Route::get('/customer/remove_session', 'CustomController@removeSession')->middleware('permission:' . Acl::PERMISSION_CUSTOMER_MANAGE);

        Route::get('/tantous', 'TantouController@index')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::post('/tantou/create', 'TantouController@create')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::post('/tantou/store', 'TantouController@store')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::get('/tantou/edit/{id}', 'TantouController@edit')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::get('/tantou/getEdit/{id}', 'TantouController@getEdit')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::post('/tantou/show/{id}', 'TantouController@show')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::put('/tantou/update/{id}', 'TantouController@update')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::delete('/tantou/delete/{id}', 'TantouController@destroy')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
        Route::get('/tantou/remove_session', 'TantouController@removeSession')->middleware('permission:' . Acl::PERMISSION_TANTOU_MANAGE);
    });

    /* For Client */
    Route::post('client/login', 'AuthController@loginClient');
    Route::group(['middleware' => 'auth:client'], function () {
        Route::post('client/logout', 'AuthController@logoutClient');

        Route::get('/client', function (Request $request) {
            return new UserResource(auth()->guard('client')->user());
        });
    });

    Route::apiResource('users', 'UserController');
    Route::apiResource('clients', 'ClientController');
    Route::apiResource('nouhinshousmeisai', 'NouhinshouMeisaiController');
    Route::apiResource('nouhinshous', 'NouhinshouController');

    Route::get('csvs', 'NouhinshouController@getCSVs');
    Route::get('list_csv1', 'NouhinshouController@listCSVType1');
    Route::get('list_csv2', 'NouhinshouController@listCSVType2');
    Route::get('pdf_view1/{id}', 'NouhinshouController@pdfView1');
    Route::get('pdf_view2/{id}', 'NouhinshouController@pdfView2');
    Route::post('/import/csv/type1', 'ImportController@importCsvType1');
    Route::post('/import/csv/type2', 'ImportController@importCsvType2');
    Route::get('/import/log', 'ImportController@getListImportLogs');
    Route::delete('/import/log/{id}', 'ImportController@removeLog');
});

// Fake APIs
Route::get('/table/list', function () {
    $rowsNumber = mt_rand(20, 30);
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'author' => Faker::randomString(mt_rand(5, 10)),
            'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
            'id' => mt_rand(100000, 100000000),
            'pageviews' => mt_rand(100, 10000),
            'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
            'title' => Faker::randomString(mt_rand(20, 50)),
        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data]));
});

Route::get('/orders', function () {
    $rowsNumber = 8;
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'order_no' => 'LARAVUE' . mt_rand(1000000, 9999999),
            'price' => mt_rand(10000, 999999),
            'status' => Faker::randomInArray(['success', 'pending']),
        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data]));
});

Route::get('/articles', function () {
    $rowsNumber = 10;
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'id' => mt_rand(100, 10000),
            'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
            'title' => Faker::randomString(mt_rand(20, 50)),
            'author' => Faker::randomString(mt_rand(5, 10)),
            'comment_disabled' => Faker::randomBoolean(),
            'content' => Faker::randomString(mt_rand(100, 300)),
            'content_short' => Faker::randomString(mt_rand(30, 50)),
            'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
            'forecast' => mt_rand(100, 9999) / 100,
            'image_uri' => 'https://via.placeholder.com/400x300',
            'importance' => mt_rand(1, 3),
            'pageviews' => mt_rand(10000, 999999),
            'reviewer' => Faker::randomString(mt_rand(5, 10)),
            'timestamp' => Faker::randomDateTime()->getTimestamp(),
            'type' => Faker::randomInArray(['US', 'VI', 'JA']),

        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data, 'total' => mt_rand(1000, 10000)]));
});

Route::get('articles/{id}', function ($id) {
    $article = [
        'id' => $id,
        'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
        'title' => Faker::randomString(mt_rand(20, 50)),
        'author' => Faker::randomString(mt_rand(5, 10)),
        'comment_disabled' => Faker::randomBoolean(),
        'content' => Faker::randomString(mt_rand(100, 300)),
        'content_short' => Faker::randomString(mt_rand(30, 50)),
        'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
        'forecast' => mt_rand(100, 9999) / 100,
        'image_uri' => 'https://via.placeholder.com/400x300',
        'importance' => mt_rand(1, 3),
        'pageviews' => mt_rand(10000, 999999),
        'reviewer' => Faker::randomString(mt_rand(5, 10)),
        'timestamp' => Faker::randomDateTime()->getTimestamp(),
        'type' => Faker::randomInArray(['US', 'VI', 'JA']),

    ];

    return response()->json(new JsonResponse($article));
});

Route::get('articles/{id}/pageviews', function ($id) {
    $pageviews = [
        'PC' => mt_rand(10000, 999999),
        'Mobile' => mt_rand(10000, 999999),
        'iOS' => mt_rand(10000, 999999),
        'android' => mt_rand(10000, 999999),
    ];
    $data = [];
    foreach ($pageviews as $device => $pageview) {
        $data[] = [
            'key' => $device,
            'pv' => $pageview,
        ];
    }

    return response()->json(new JsonResponse(['pvData' => $data]));
});
