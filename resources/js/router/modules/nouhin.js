/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const nouhinRoutes = {
  path: '/nouhin',
  component: Layout,
  redirect: '/nouhin/index',
  name: 'Nouhinshous',
  meta: {
    title: 'nouhin',
    icon: 'shopping',
    permissions: ['view menu nouhinshous'],
  },
  children: [
    {
      path: 'create',
      component: () => import('@/views/nouhin/Create'),
      name: 'CreateNouhin',
      meta: { title: 'createNouhin', icon: 'edit', permissions: ['manage nouhinshous'] },
      hidden: true,
    },
    {
      path: 'edit/:id(\\d+)',
      component: () => import('@/views/nouhin/Edit'),
      name: 'EditNouhin',
      meta: { title: 'editNouhin', noCache: true, permissions: ['manage nouhinshous'] },
      hidden: true,
    },
    {
      path: 'index',
      component: () => import('@/views/nouhin/List'),
      name: 'ListNouhinshous',
      meta: { title: 'nouhin', icon: 'documentation', permissions: ['manage nouhinshous'] },
    },
  ],
};

export default nouhinRoutes;
