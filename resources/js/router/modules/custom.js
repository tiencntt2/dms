/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const customRoutes = {
  path: '/customer',
  component: Layout,
  redirect: '/customer',
  meta: {
    title: 'customer',
    icon: 'user',
    permissions: ['view menu customer'],
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/customer/index.vue'),
      name: 'customerIndex',
      meta: { title: 'customerIndex', permissions: ['manage customer'] },
    },
    {
      path: 'add',
      component: () => import('@/views/customer/add.vue'),
      name: 'customerAdd',
      meta: { title: 'customerAdd', permissions: ['manage customer'] },
      hidden: true,
    },
    {
      path: 'edit/:id',
      component: () => import('@/views/customer/edit.vue'),
      name: 'customerEdit',
      meta: { title: 'customerEdit', permissions: ['manage customer'], noCache: true },
      hidden: true,
    },
    {
      path: 'confirm/:id',
      component: () => import('@/views/customer/confirm.vue'),
      name: 'customerConfirm',
      meta: { title: 'customerConfirm', permissions: ['manage customer'] },
      hidden: true,
    },
  ],
};

export default customRoutes;
