/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const tantouRoutes = {
  path: '/tantou',
  component: Layout,
  redirect: '/tantou',
  meta: {
    title: 'tantou',
    icon: 'people',
    permissions: ['view menu tantou'],
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/tantou/index.vue'),
      name: 'tantouIndex',
      meta: { title: 'tantouIndex', permissions: ['manage tantou'] },
    },
    {
      path: 'add',
      component: () => import('@/views/tantou/add.vue'),
      name: 'tantouAdd',
      meta: { title: 'tantouAdd', permissions: ['manage tantou'] },
      hidden: true,
    },
    {
      path: 'edit/:id',
      component: () => import('@/views/tantou/edit.vue'),
      name: 'tantouEdit',
      meta: { title: '社内担当詳細', permissions: ['manage tantou'], noCache: true },
      hidden: true,
    },
    {
      path: 'confirm/:id',
      component: () => import('@/views/tantou/confirm.vue'),
      name: 'tantouConfirm',
      meta: { title: '社内担当詳細・登録 [確認]', permissions: ['manage tantou'] },
      hidden: true,
    },
  ],
};

export default tantouRoutes;
