import Cookies from 'js-cookie';

const TokenKey = 'isLogged';
const ClientTokenKey = 'isClientLogged';

export function isLogged() {
  return Cookies.get(TokenKey) === '1';
}

export function setLogged(isLogged) {
  return Cookies.set(TokenKey, isLogged);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}

export function isClientLogged() {
  return Cookies.get(ClientTokenKey) === '1';
}

export function setClientLogged(isLogged) {
  return Cookies.set(ClientTokenKey, isLogged);
}

export function removeClientToken() {
  return Cookies.remove(ClientTokenKey);
}
