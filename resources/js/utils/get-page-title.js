import defaultSettings from '@/settings';
import i18n from '@/lang';

const title = defaultSettings.title || 'パナメディカル株式会社 納品書発行システム';

export default function getPageTitle(key) {
  const hasKey = i18n.te(`route.${key}`);
  if (hasKey) {
    const pageName = i18n.t(`route.${key}`);
    return `${pageName} - ${title}`;
  }
  return `${title}`;
}
