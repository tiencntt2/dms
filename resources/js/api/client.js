import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/clients',
    method: 'get',
    params: query,
  });
}

export function fetchClient(id) {
  return request({
    url: '/clients/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/clients/' + id + '/pageviews',
    method: 'get',
  });
}

export function createClient(data) {
  return request({
    url: '/clients/create',
    method: 'post',
    data,
  });
}

export function updateClient(data) {
  return request({
    url: '/clients/update',
    method: 'post',
    data,
  });
}
