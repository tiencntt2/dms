import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/nouhinshousmeisai',
    method: 'get',
    params: query,
  });
}

export function fetchNouhinshou(id) {
  return request({
    url: '/nouhinshousmeisai/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/nouhinshousmeisai/' + id + '/pageviews',
    method: 'get',
  });
}

export function createNouhinshou(data) {
  return request({
    url: '/nouhinshousmeisai/create',
    method: 'post',
    data,
  });
}

export function updateNouhinshou(data) {
  return request({
    url: '/nouhinshousmeisai/update',
    method: 'post',
    data,
  });
}
