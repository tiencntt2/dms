import request from '@/utils/request';

export function importCsvType1(data) {
  return request({
    url: '/import/csv/type1',
    method: 'post',
    data,
  });
}

export function importCsvType2(data) {
  return request({
    url: '/import/csv/type2',
    method: 'post',
    data,
  });
}
