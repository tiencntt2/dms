import request from '@/utils/request';
import Resource from '@/api/resource';

class customResource extends Resource {
  constructor() {
    super('client');
  }

  list(query) {
    return request({
      url: '/customers',
      method: 'get',
      params: query,
    });
  }

  create(data) {
    return request({
      url: '/customer/create',
      method: 'post',
      data,
    });
  }

  store(data) {
    return request({
      url: '/customer/store',
      method: 'post',
      data,
    });
  }

  get(id) {
    return request({
      url: '/customer/edit/' + id,
      method: 'get',
    });
  }

  getEdit(id) {
    return request({
      url: '/customer/getEdit/' + id,
      method: 'get',
    });
  }

  removeSession() {
    return request({
      url: '/customer/remove_session',
      method: 'get',
    });
  }

  show(id, data) {
    return request({
      url: '/customer/show/' + id,
      method: 'post',
      data,
    });
  }

  confirm(id) {
    return request({
      url: '/customer/edit/' + id,
      method: 'get',
    });
  }

  update(id, resource) {
    return request({
      url: '/customer/update/' + id,
      method: 'put',
      data: resource,
    });
  }

  destroy(id) {
    return request({
      url: '/customer/delete/' + id,
      method: 'delete',
    });
  }
}

export { customResource as default };
