import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/nouhinshous',
    method: 'get',
    params: query,
  });
}

export function fetchNouhinshou(id) {
  return request({
    url: '/nouhinshous/' + id,
    method: 'get',
  });
}

export function fetchPv(id) {
  return request({
    url: '/nouhinshous/' + id + '/pageviews',
    method: 'get',
  });
}

export function createNouhinshou(data) {
  return request({
    url: '/nouhinshous/create',
    method: 'post',
    data,
  });
}

export function updateNouhinshou(data) {
  return request({
    url: '/nouhinshous/update',
    method: 'post',
    data,
  });
}
