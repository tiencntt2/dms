import request from '@/utils/request';
import Resource from '@/api/resource';

class tantouResource extends Resource {
  constructor() {
    super('user');
  }

  list(query) {
    return request({
      url: '/tantous',
      method: 'get',
      params: query,
    });
  }

  create(data) {
    return request({
      url: '/tantou/create',
      method: 'post',
      data,
    });
  }

  store(data) {
    return request({
      url: '/tantou/store',
      method: 'post',
      data,
    });
  }

  get(id) {
    return request({
      url: '/tantou/edit/' + id,
      method: 'get',
    });
  }

  getEdit(id) {
    return request({
      url: '/tantou/getEdit/' + id,
      method: 'get',
    });
  }

  removeSession() {
    return request({
      url: '/tantou/remove_session',
      method: 'get',
    });
  }

  show(id, data) {
    return request({
      url: '/tantou/show/' + id,
      method: 'post',
      data,
    });
  }

  update(id, resource) {
    return request({
      url: '/tantou/update/' + id,
      method: 'put',
      data: resource,
    });
  }

  destroy(id) {
    return request({
      url: '/tantou/delete/' + id,
      method: 'delete',
    });
  }
}

export { tantouResource as default };
