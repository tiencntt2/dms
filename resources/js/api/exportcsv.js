import request from '@/utils/request';
import Resource from '@/api/resource';

class customResource extends Resource {
  constructor() {
    super('');
  }

  listCSVs(query) {
    return request({
      url: '/csvs',
      method: 'get',
      params: query,
    });
  }

  listCSV1(query) {
    return request({
      url: '/list_csv1',
      method: 'get',
      params: query,
    });
  }

  listCSV2(query) {
    return request({
      url: '/list_csv2',
      method: 'get',
      params: query,
    });
  }

  getViewPDF(id) {
    return request({
      url: '/pdf_view1/' + id,
      method: 'get',
    });
  }

  getViewPDF2(id) {
    return request({
      url: '/pdf_view2/' + id,
      method: 'get',
    });
  }
}

export { customResource as default };
