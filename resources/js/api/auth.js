import request from '@/utils/request';

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: data,
  });
}
export function loginClient(data) {
  return request({
    url: '/client/login',
    method: 'post',
    data: data,
  });
}

export function getInfo(token) {
  return request({
    url: '/user',
    method: 'get',
  });
}

export function getInfoClient(token) {
  return request({
    url: '/client',
    method: 'get',
  });
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post',
  });
}

export function logoutClient() {
  return request({
    url: '/client/logout',
    method: 'post',
  });
}

export function csrf() {
  return request({
    url: '/sanctum/csrf-cookie',
    method: 'get',
  });
}

export function resetPassword(data) {
  return request({
    url: '/auth/reset-password',
    method: 'post',
    data: data,
  });
}

export function resetClientPassword(data) {
  return request({
    url: '/client/reset-password',
    method: 'post',
    data: data,
  });
}
