import { login, loginClient, logout, logoutClient, getInfo, getInfoClient, resetPassword, resetClientPassword } from '@/api/auth';
import { isLogged, isClientLogged, setLogged, setClientLogged, removeToken, removeClientToken } from '@/utils/auth';
import router, { resetRouter } from '@/router';
import store from '@/store';

const state = {
  id: null,
  user: null,
  token: isLogged(),
  client: isClientLogged(),
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  permissions: [],
};

const mutations = {
  SET_ID: (state, id) => {
    state.id = id;
  },
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_CLIENT_TOKEN: (state, client) => {
    state.client = client;
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction;
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions;
  },
};

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { loginID, password } = userInfo;
    return new Promise((resolve, reject) => {
      login({ login_id: loginID.trim(), password: password })
        .then(response => {
          setLogged('1');
          setClientLogged('0');
          resolve();
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  },

  // client login
  loginClient({ commit }, userInfo) {
    const { loginID, password } = userInfo;
    return new Promise((resolve, reject) => {
      loginClient({ login_id: loginID.trim(), password: password })
        .then(response => {
          setLogged('0');
          setClientLogged('1');
          resolve();
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo()
        .then(response => {
          const { data } = response;

          if (!data) {
            reject('Verification failed, please Login again.');
          }

          const { roles, name, avatar, introduction, permissions, id } = data;
          // roles must be a non-empty array
          if (!roles || roles.length <= 0) {
            reject('getInfo: roles must be a non-null array!');
          }

          commit('SET_ROLES', roles);
          commit('SET_PERMISSIONS', permissions);
          commit('SET_NAME', name);
          commit('SET_AVATAR', avatar);
          commit('SET_INTRODUCTION', introduction);
          commit('SET_ID', id);
          resolve(data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  // get client info
  getInfoClient({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfoClient()
        .then(response => {
          const { data } = response;

          if (!data) {
            reject('Verification failed, please Client Login again.');
          }

          const { roles, name, avatar, introduction, permissions, id } = data;
          // roles must be a non-empty array
          if (!roles || roles.length <= 0) {
            reject('getInfo: roles must be a non-null array!');
          }

          commit('SET_ROLES', roles);
          commit('SET_PERMISSIONS', permissions);
          commit('SET_NAME', name);
          commit('SET_AVATAR', avatar);
          commit('SET_INTRODUCTION', introduction);
          commit('SET_ID', id);
          resolve(data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      logout()
        .then(() => {
          commit('SET_TOKEN', '');
          commit('SET_ROLES', []);
          removeToken();
          resetRouter();
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      resolve();
    });
  },

  // client logout
  logoutClient({ commit }) {
    return new Promise((resolve, reject) => {
      logoutClient()
        .then(() => {
          commit('SET_CLIENT_TOKEN', '');
          commit('SET_ROLES', []);
          removeClientToken();
          resetRouter();
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // remove token
  resetTokenClient({ commit }) {
    return new Promise(resolve => {
      commit('SET_CLIENT_TOKEN', '');
      commit('SET_ROLES', []);
      removeClientToken();
      resolve();
    });
  },

  // Dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      // const token = role + '-token';

      // commit('SET_TOKEN', token);
      // setLogged(token);

      // const { roles } = await dispatch('getInfo');

      const roles = [role.name];
      const permissions = role.permissions.map(permission => permission.name);
      commit('SET_ROLES', roles);
      commit('SET_PERMISSIONS', permissions);
      resetRouter();

      // generate accessible routes map based on roles
      const accessRoutes = await store.dispatch('permission/generateRoutes', { roles, permissions });

      // dynamically add accessible routes
      router.addRoutes(accessRoutes);

      resolve();
    });
  },

  // reset password for admin
  resetPassword({ commit }, userInfo) {
    const { loginID, email } = userInfo;
    return new Promise((resolve, reject) => {
      resetPassword({ login_id: loginID.trim(), email: email })
        .then(response => {
          console.log('sent email');
          resolve();
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  },

  // reset password for admin
  resetClientPassword({ commit }, userInfo) {
    const { loginID, email } = userInfo;
    return new Promise((resolve, reject) => {
      resetClientPassword({ login_id: loginID.trim(), email: email })
        .then(response => {
          console.log('sent email');
          resolve();
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
