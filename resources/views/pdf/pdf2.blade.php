<!DOCTYPE html>
<html lang="ja-JP">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift-JIS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <span class="fs12">{{$nouhinshou['created_at']}}</span>--}}
{{--    <span class="fs12 ml270">{{$nouhinshou['denpyo_number']}}</span>--}}
    <title>納品書</title>

    <style>
        body {
            font-size: 11px;
            -webkit-print-color-adjust: exact;
        }

        table,
        th,
        td {
            border-collapse: collapse;
            border-spacing: 0;
        }

        * {
            font-family: sans-serif;
        }

        .h3 {
            margin-top: 0;
            margin-bottom: 5px;
        }

        .borderbox td,
        .borderbox th {
            font-size: 11px;
            border-left: 1px solid #000;
            border-right: 1px solid #000;
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }

        .meisairow {
            height: 40px;
            padding: 3px 3px 3px 10px;
        }

        .borderbox .noneborder {
            border: none !important;
        }

        .basepadding * {
            padding: 4px;
        }

        .caution {
            padding-top: 5px;
            font-size: 10px;
            line-height: 1.2;
        }

        .ml270 {
            margin-left: 300px;
        }

        .c666{
            color: #666;
        }

        .mb25 {
            margin-bottom: 8px;
        }

        .mb10 {
            margin-bottom: 5px;
        }

        strong {
            font-size: 14px;
            font-weight: bold;
            display: block;
            margin-bottom: 10px;
        }

        strong img {
            margin-right: 10px;
        }

        th {
            background-color: #ccc;
        }

        h1 {
            text-align: center;
        }

        body .container {
            width: 620px;
            margin: 0 auto;
        }

        .fl {
            float: left;
        }

        .w10 {
            width: 10%;
        }

        .w20 {
            width: 20%;
        }

        .w25 {
            width: 25%;
        }

        .w30 {
            width: 30%;
        }

        .w35 {
            width: 35%;
        }

        .w44 {
            width: 44%;
        }

        .w45 {
            width: 45%;
        }

        .w50 {
            width: 50%;
        }

        .w100 {
            width: 100%;
        }

        .of3 {
            margin-right: 3%;
        }

        .of20 {
            margin-right: 20%;
        }

        .of75left {
            margin-left: 75%;
        }

        #textaddress {
            font-size: 13px;
            min-height: 80px;
        }

        #stamp th {
            font-size: 9px;
            text-align: center;
        }

        #stamp td {
            padding: 10px 0;
            text-align: center;
        }

        #number {
            border-bottom: 2px solid #000;
            margin-bottom: 10px;
            font-size: 12px;
            line-height: 1.1;
        }

        #haisou {
            border-bottom: 2px solid #000;
            font-size: 11px;
        }

        #myaddress {
            line-height: 1.4;
        }

        .text-ontyuu {
            font-size: 11px;
            text-align: right;
            font-weight: bold;
        }

        .text-ontyuu.bottom {
            border-bottom: 2px solid #000;
        }

        .text-ontyuu span {
            padding-left: 10px;
            white-space: nowrap;
        }

        .bottomframe {
            position: relative;
            height: 60px;
        }

        .bottom {
            width: 100%;
            position: absolute;
            bottom: 0px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }

        .row {
            clear: both;
            overflow: hidden;
        }

        #bikou {
            box-sizing: border-box;
            min-height: 60px;
            border: 2px solid #000;
            padding: 4px;
            font-size: 11px;
            line-height: 1.4;
            margin-bottom: 0px;
        }

        #bikou span.title {
            padding-right: 1em;
        }

        .fs12 {
            font-size: 12px;
        }

        .fs16 {
            font-size: 16px;
        }

        .torihikiDt {
            text-align: right;
            margin: 0;
        }
    </style>
</head>
<body>
<div class="container">
    <p class="h3 text-center mb10">納品書</p>
    <div class="row">
        <div id="textaddress" class="w45 of20 mb25 fl bottomframe"></div>
        <div id="myaddress" class="w35 mb25 fl fs12 c666">
            <strong><img src="./images/logo.png" alt="logo" width="200"></strong>
            〒105-0014 東京都港区芝1-5-11<br>
            TEL:03-5440-7373(代)<br>
            FAX:03-5440-7377
        </div>
    </div>
    <div class="row">
        <div class="w25 fl of75left">
            <p id="torihikiDt" class="torihikiDt fs12 c666"><span>{{$nouhinshou['torihiki_datetime']}}</span></p>
            <p id="number" class="mb25">No.{{$nouhinshou['denpyo_number']}}</p>
        </div>
    </div>
    <div class="row">
        <div class="w44 of3 mb25 fl bottomframe">
            <div class="text-ontyuu bottom">
                <div class="mb25 fs16">{{$nouhinshou['nonyusaki_name']}}<span></span></div>
                <div class="fs16">{{$nouhinshou['seikyusaki_name']}}<span></span></div>
            </div>
        </div>
        <div class="w25 of3 mb25 fl bottomframe">
            <div id="haisou" class="bottom"><span>配送方法:</span></div>
        </div>
        <div class="w25 mb25 fl">
            <table id="stamp" class="w100 borderbox basepadding">
                <tbody>
                <tr>
                    <th class="w50">お客様コード</th>
                    <th class="w50">担当者</th>
                </tr>
                <tr>
                    <td class="w50">{{$nouhinshou['nonyusaki_code']}}</td>
                    <td class="w50">{{$nouhinshou['tantou_code']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div class="row">
        <table class="borderbox basepadding w100 mb25">
            <tbody>
            <tr>
                <th class="w30 text-center">商品名</th>
                <th class="w10 text-center">数量</th>
                <th class="w10 text-center">単価</th>
                <th class="w20 text-center">金額</th>
                <th class="w30 text-center">摘要</th>
            </tr>
            <tr>
                <td class="text-right meisairow">{{$nouhinshou['torihiki_text']}}</td>
                <td class="text-right meisairow"></td>
                <td class="text-left meisairow"></td>
                <td class="text-right meisairow"></td>
                <td class="text-right meisairow"></td>
            </tr>
            @foreach ($nouhinshou['nouhinshou_meisais'] as $meisais)
                @if ($meisais['meisai_line'] != 90)
                    <tr>
                        <td class="text-right meisairow">{{$meisais['item_name']}}</td>
                        <td class="text-right meisairow">{{$meisais['item_quantity']}}</td>
                        <td class="text-left meisairow">{{$meisais['item_tanka']}}</td>
                        <td class="text-right meisairow">{{$meisais['kingaku']}}</td>
                        <td class="text-right meisairow">{{$meisais['tekiyo1']}}{{$meisais['tekiyo1'] ? ($meisais['tekiyo2'] ? ' ,' : '') : ''}}{{$meisais['tekiyo2']}}</td>
                    </tr>
                @endif
                @if ($meisais['meisai_line'] == 90)
                    <tr>
                        <th class="text-right">合計</th>
                        <td class="text-right">{{$meisais['item_quantity']}}</td>
                        <th class="text-right">納品計</th>
                        <td class="text-right">{{$meisais['kingaku']}}</td>
                        <td class="noneborder"></td>
                    </tr>
                    <tr>
                        <td class="noneborder"></td>
                        <td class="noneborder"></td>
                        <th class="text-right">消費税額</th>
                        <td class="text-right">{{$meisais['zei']}}</td>
                        <td class="noneborder"></td>
                    </tr>
                    <tr>
                        <td class="noneborder"></td>
                        <td class="noneborder"></td>
                        <th class="text-right">合計</th>
                        <td class="text-right">{{$meisais['total']}}</td>
                        <td class="noneborder"></td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <p id="bikou" class="borderbox w100">
            <span class="title">備考</span>
            <br>
            {{$nouhinshou['biko_text']}}
        </p>

    </div>
    <p class="caution">※本伝票に関してご不審の点がございましたら、発行期日より一週間以内にお問い合わせください。</p>
</div>
</body>
</html>
