<!DOCTYPE html>
<!-- saved from url=(0083)https://sys.panamd.com/client/printhtml/index.php?ret=&nohinshoId=2372938&mode=edit -->
<html lang="ja-JP"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UA53462</title>
    <link href="" rel="stylesheet" media="all">

    <style>
        body {font-size: 11px;-webkit-print-color-adjust: exact;}
        table,
        th,
        td {
            border-collapse: collapse;
            border-spacing: 0;
        }
        * {
            font-family: sans-serif;
        }
        .h3 {margin-top: 0; margin-bottom: 5px;}
        .borderbox td,
        .borderbox th {
            font-size: 11px;
            border-left:1px solid #000;
            border-right:1px solid #000;
            border-top:1px solid #000;
            border-bottom:1px solid #000;
        }

        .meisairow{
            height: 40px;
            padding:3px 3px 3px 10px;
        }

        .borderbox .noneborder { border: none!important;}
        .borderbox .noneborder_r { border-right: none!important;}
        .borderbox .noneborder_l { border-left: none!important;}
        .borderbox .noneborder_t { border-top: none!important;}
        .borderbox .noneborder_b { border-bottom: none!important;}
        .basepadding * { padding: 4px;}

        .caution {padding-top:5px; font-size: 10px; line-height: 1.2; }
        .mleft { margin-left: -15px;}
        .mt20 {margin-top:20px;}
        .mb25 {margin-bottom:8px;}
        .mb10 {margin-bottom: 5px;}
        strong {font-size: 14px; font-weight: bold; display: block; margin-bottom: 10px; }
        strong img {margin-right: 10px;}
        th { background-color: #ccc; }

        h1 { text-align: center;}
        body .container {width: 820px;}
        .fl { float: left;}
        .fr { float: right;}
        .w5 {width: 5%;}
        .w10 {width: 10%;}
        .w20 {width: 20%;}
        .w25 {width: 25%;}
        .w30 {width: 30%;}
        .w35 {width: 35%;}
        .w40 {width: 40%;}
        .w42 {width: 42%;}
        .w44 {width: 44%;}
        .w45 {width: 45%;}
        .w50 {width: 50%;}
        .w60 {width: 60%;}
        .w70 {width: 70%;}
        .w80 {width: 80%;}
        .w100 {width: 100%;}
        .of3 {margin-right: 3%;}
        .of5 {margin-right: 5%;}
        .of10 {margin-right: 10%;}
        .of20 {margin-right: 20%;}
        .of30 {margin-right: 30%;}
        .of40 {margin-right: 40%;}
        .of15left {margin-left: 15%;}
        .of75left {margin-left: 75%;}
        .of80left {margin-left: 80%;}
        .of10top {margin-top: 10%;}

        #textaddress {font-size: 13px; min-height: 80px;}
        #stamp th { font-size: 9px; text-align: center;}
        #stamp td { padding: 10px 0; text-align: center;}
        #number {border-bottom: 2px solid #000; margin-bottom: 10px; font-size: 12px; line-height: 1.1;}
        #haisou {border-bottom: 2px solid #000; font-size: 11px;}
        #myaddress { line-height: 1.4; }
        .text-ontyuu { font-size: 11px;}
        .text-ontyuu { font-size: 11px;}
        .text-ontyuu.bottom { border-bottom: 2px solid #000;}
        .text-ontyuu span {padding-left: 10px; white-space: nowrap;}
        .bottomframe{
            position: relative;
            height: 30px;
        }
        .bottom{
            width: 100%;
            position: absolute;
            bottom: 0px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }
        .row {
            clear:both;
            overflow: hidden;
        }
        #bikou { box-sizing: border-box; min-height: 60px; border: 2px solid #000; padding: 4px; font-size: 11px; line-height: 1.4; margin-bottom: 0px;}
        #bikou span.title { padding-right: 1em;}

        .fs14{
            font-size:14px;
        }
        .fs16{
            font-size:16px;
        }
        .fs18{
            font-size:18px;
        }
        .torihikiDt{
            text-align:right;
            margin:0;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="h3 text-center mb10 mt20">納品書</h1>
        <div class="row">
            <div class="w30 mb25 fl">
                <div class="fs14"><span>〒105-0014</span></div>
                <div class="fs14"><span>東京都港区芝1-5-11  東京都港区芝1-5-11</span></div>
                <div class="fs14"><span>取引日 2021年 6月21日</span></div>
            </div>
            <div class="w40 mt20 fl">
                <table id="stamp" class="w100 borderbox basepadding">
                    <tbody><tr>
                        <th class="w20">お客様コード</th>
                        <th class="w20">担当者</th>
                        <th class="w20">発注書番号</th>
                        <th class="w20">納品書番号</th>
                        <th class="w20">配信方法</th>
                    </tr>
                    <tr>
                        <td class="w20">{{$nouhinshou['id']}}</td>
                        <td class="w20">{{$nouhinshou['id']}}</td>
                        <td class="w20">{{$nouhinshou['id']}}</td>
                        <td class="w20">701</td>
                        <td class="w20"></td>
                    </tr>
                    </tbody></table>
            </div>
            <div class="w30 mb25 fr">
                <div class="fs16 of15left"><span>取引日 2021年 6月21日</span></div>
                <div id="myaddress" class="w70 mb25 of15left">
                    <strong><img src="./UA53462_files/logo.png" alt="logo" width="200"></strong>
                    〒105-0014 東京都港区芝1-5-11<br>
                    TEL:03-5440-7373(代)<br>
                    FAX:03-5440-7377
                </div>
            </div>
        </div>
        <div class="row">
            <div class="w35 of3 mb25 fl bottomframe">
                <div class="text-ontyuu bottom">
                    <div class="fs16">メガネハウス御中<span></span></div>
                </div>
            </div>
            <div class="w25 of3 mb25 fl bottomframe"><div class="fs16 bottom"><span>売上</span></div></div>

        </div>


        <div class="row">
            <table class="borderbox basepadding w100 mb25">
                <tbody><tr>
                    <th class="w10 text-center">品目コード</th>
                    <th class="w20 text-center">品名  品番</th>
                    <th class="w10 text-center">ｸﾗｽ分類  特定保存</th>
                    <th class="w10 text-center">数量</th>
                    <th class="w10 text-center">単価</th>
                    <th class="w10 text-center">金額</th>
                    <th class="w10 text-center">摘要</th>
                    <th class="w20 text-center">JANコード</th>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">531柴田</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">4033園田</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">3664岩渕</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">4011寺尾</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">3028三島</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">ﾒｶﾞﾈﾊｳｽ ﾎﾟｲﾝﾄｶｰﾄﾞ値引</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow">3359渡邊</td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow">0</td>
                    <td class="text-right meisairow">0</td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-right meisairow"></td>
                    <td class="text-left meisairow"></td>
                    <td class="text-right meisairow"></td>
                </tr>
                <tr>
                    <td class="text-right meisairow" colspan="2"></td>
                    <th class="text-right">合計</th>
                    <td class="text-right">0</td>
                    <th class="text-right">納品計</th>
                    <td class="text-right">-5,455</td>

                    <td class="text-right meisairow" colspan="2" rowspan="3"></td>
                </tr>
                <tr>
                    <td class="text-left meisairow" colspan="4" rowspan="2">
                        <span class="title">備考</span>
                    </td>
                    <th class="text-right">消費税額</th>
                    <td class="text-right">-545</td>
                </tr>
                <tr>
                    <th class="text-right">合計</th>
                    <td class="text-right">-6,000</td>
                </tr>
                </tbody></table>
        </div>


        <p class="caution">※本伝票に関してご不審の点がございましたら、発行期日より一週間以内にお問い合わせください。</p>
    </div>
    <script>
        import Resource from '@/api/resource';
        const nouhinshouResource = new Resource('nouhinshous');
        export default {
            created() {
                this.getCustom();
            },
        }
    </script>
</body>
</html>
