<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'login_id' => Str::random(10),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'code' => "TEST" . Str::random(4),
        'email' => $faker->unique()->safeEmail,
        'order' => "0000" . Str::upper($faker->words),
        'role_name' => \App\Faker::randomInArray([\App\Acl::ROLE_ADMIN, \App\Acl::ROLE_MANAGER, \App\Acl::ROLE_USER,]),
        'updated_user' => $faker->randomNumber(2)
    ];
});
