<?php
namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use App\Acl;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientList = [
            "Albert Einstein",
            "Anna K. Behrensmeyer",
            "Blaise Pascal",
            "Caroline Herschel",
            "Cecilia Payne-Gaposchkin",
            "Chien-Shiung Wu",
            "Dorothy Hodgkin",
            "Edmond Halley",
            "Edwin Powell Hubble",
            "Elizabeth Blackburn",
            "Enrico Fermi",
            "Erwin Schroedinger",
            "Flossie Wong-Staal",
            "Frieda Robscheit-Robbins",
            "Geraldine Seydoux",
            "Gertrude B. Elion",
            "Ingrid Daubechies",
            "Jacqueline K. Barton",
            "Jane Goodall",
            "Jocelyn Bell Burnell",
            "Johannes Kepler",
            "Lene Vestergaard Hau",
            "Lise Meitner",
            "Lord Kelvin",
            "Maria Mitchell",
            "Marie Curie",
            "Max Born",
            "Max Planck",
            "Melissa Franklin",
            "Michael Faraday",
            "Mildred S. Dresselhaus",
            "Nicolaus Copernicus",
            "Niels Bohr",
            "Patricia S. Goldman-Rakic",
            "Patty Jo Watson",
            "Polly Matzinger",
            "Richard Phillips Feynman",
            "Rita Levi-Montalcini",
            "Rosalind Franklin",
            "Ruzena Bajcsy",
            "Sarah Boysen",
            "Shannon W. Lucid",
            "Shirley Ann Jackson",
            "Sir Ernest Rutherford",
            "Sir Isaac Newton",
            "Stephen Hawking",
            "Werner Karl Heisenberg",
            "Wilhelm Conrad Roentgen",
            "Wolfgang Ernst Pauli",
        ];

        $faker = \Faker\Factory::create('vi_VN');

        $client_demo = Client::create([
            'name' => 'Omo C. Ocampo Uria',
            'login_id' => 35017103,
            'password' => Hash::make('laravue'),
            'login_id2' => 35017104,
            'password2' => Hash::make('laravue2'),
            'code' => $faker->username,
            'email' => 'omo.ocampo.uria@laravue.dev',
            'order' => 1,
            'role' => Acl::ROLE_USER,
            'type' => rand(1,2),
            'updated_user' => 2,
            'deleted_flg' => 0
        ]);

        $role_demo = Role::findByName(Acl::ROLE_USER);
        $client_demo->syncRoles($role_demo);

        foreach ($clientList as $fullName) {
            $name = str_replace(' ', '.', $fullName);
            $roleName = Acl::ROLE_USER;
            $client = Client::create([
                'name' => $fullName,
                'login_id' => $faker->unique()->ean8,
                'password' => Hash::make('laravue'),
                'login_id2' => $faker->unique()->ean8.'2',
                'password2' => Hash::make('laravue2'),
                'code' => $faker->text($maxNbChars = 9) . rand(1, 9),
                'email' => strtolower($name) . '@laravue.dev',
                'order' => rand(1,66),
                'role' => $roleName,
                'type' => rand(1,2),
                'updated_user' => rand(1,66),
                'deleted_flg' => 0
            ]);

            $role = Role::findByName($roleName);
            $client->syncRoles($role);
        }
    }
}
