<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Acl;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@laravue.dev',
            'password' => Hash::make('laravue'),
            'login_id' => 'fakeadmin1',
            'code' => 'FAKEADMIN',
            'order' => 1,
            'role' => \App\Acl::ROLE_ADMIN,
            'updated_user' => 1
        ]);
        $manager = User::create([
            'name' => 'Manager',
            'email' => 'manager@laravue.dev',
            'password' => Hash::make('laravue'),
            'login_id' => 'fakemanager1',
            'code' => 'FAKEMANAGE',
            'order' => 2,
            'role' => \App\Acl::ROLE_MANAGER,
            'updated_user' => 1
        ]);

        $adminRole = Role::findByName(\App\Acl::ROLE_ADMIN);
        $managerRole = Role::findByName(\App\Acl::ROLE_MANAGER);
        $admin->syncRoles($adminRole);
        $manager->syncRoles($managerRole);

        $userList = [
            "Adriana C. Ocampo Uria",
            "Albert Einstein",
            "Anna K. Behrensmeyer",
            "Blaise Pascal",
            "Caroline Herschel",
            "Cecilia Payne-Gaposchkin",
            "Chien-Shiung Wu",
            "Dorothy Hodgkin",
            "Edmond Halley",
            "Edwin Powell Hubble",
            "Elizabeth Blackburn",
            "Enrico Fermi",
            "Erwin Schroedinger",
            "Flossie Wong-Staal",
            "Frieda Robscheit-Robbins",
            "Geraldine Seydoux",
            "Gertrude B. Elion",
            "Ingrid Daubechies",
            "Jacqueline K. Barton",
            "Jane Goodall",
            "Jocelyn Bell Burnell",
            "Johannes Kepler",
            "Lene Vestergaard Hau",
            "Lise Meitner",
            "Lord Kelvin",
            "Maria Mitchell",
            "Marie Curie",
            "Max Born",
            "Max Planck",
            "Melissa Franklin",
            "Michael Faraday",
            "Mildred S. Dresselhaus",
            "Nicolaus Copernicus",
            "Niels Bohr",
            "Patricia S. Goldman-Rakic",
            "Patty Jo Watson",
            "Polly Matzinger",
            "Richard Phillips Feynman",
            "Rita Levi-Montalcini",
            "Rosalind Franklin",
            "Ruzena Bajcsy",
            "Sarah Boysen",
            "Shannon W. Lucid",
            "Shirley Ann Jackson",
            "Sir Ernest Rutherford",
            "Sir Isaac Newton",
            "Stephen Hawking",
            "Werner Karl Heisenberg",
            "Wilhelm Conrad Roentgen",
            "Wolfgang Ernst Pauli",
        ];

        $faker = \Faker\Factory::create('vi_VN');

        foreach ($userList as $fullName) {
            $name = str_replace(' ', '.', $fullName);
            $roleName = \App\Faker::randomInArray([
                Acl::ROLE_ADMIN,
                Acl::ROLE_MANAGER
            ]);
            $user = \App\Models\User::create([
                'name' => $fullName,
                'login_id' => $faker->unique()->ean8,
                'password' => \Illuminate\Support\Facades\Hash::make('laravue'),
                'code' => $faker->text($maxNbChars = 9) . rand(1, 9),
                'email' => strtolower($name) . '@laravue.dev',
                'order' => rand(1,66),
                'role' => $roleName,
                'updated_user' => rand(1,66)
            ]);

            $role = Role::findByName($roleName);
            $user->syncRoles($role);
        }
    }
}
