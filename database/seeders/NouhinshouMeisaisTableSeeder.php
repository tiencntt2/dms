<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;

class NouhinshouMeisaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('vi_VN');

        for ($i = 0; $i < 100; $i++) {
            \App\Models\NouhinshouMeisai::create([
                'nouhinshou_id' => rand(1,5),
                'item_code' => rand(1,100),
                'item_name' => $faker->text($maxNbChars = 50),
                'item_class' => $faker->text($maxNbChars = 50),
                'item_number' => rand(1,10),
                'item_kikaku' => $faker->text($maxNbChars = 50),
                'item_quantity' => rand(1,10),
                'item_tanka' => $faker->randomFloat(),
                'kingaku' => $faker->randomFloat(),
                'zei' => rand(1,100),
                'tekiyo1' => $faker->text($maxNbChars = 20),
                'tekiyo2' => $faker->text($maxNbChars = 20),
                'meisai_line' => (($i + 1) % 5 == 0) ? 90 : (($i % 5) + 10),
                'order' => rand(1,100),
                'updated_user_id' => rand(1,3),
                'deleted_flg' => 0,
            ]);
        }
    }
}
