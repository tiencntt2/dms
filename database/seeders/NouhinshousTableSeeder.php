<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NouhinshousTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('vi_VN');

        for ($i = 0; $i < 100; $i++) {
            \App\Models\Nouhinshous::create([
                'tantou_id' => rand(1,2),
                'torihiki_code' => rand(1,100),
                'torihiki_text' => $faker->text($maxNbChars = 14),
                'nonyusaki_id' => rand(1,2),
                'nonyusaki_text' => $faker->text($maxNbChars = 200),
                'seikyusaki_id' => rand(1,2),
                'seikyusaki_text' => $faker->text($maxNbChars = 50),
                'torihiki_datetime' => '20210720',
                'denpyo_number' => $i,
                'biko_text' => $faker->text($maxNbChars = 300),
                'order_number' => rand(1,66),
                'total_kingaku' => rand(1,100),
                'updated_user_id' => rand(1,66),
                'deleted_flg' => 0
            ]);
        }
    }
}
