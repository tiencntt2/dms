<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNouhinshousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nouhinshous', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tantou_id')->nullable();
            $table->string('torihiki_code', 50)->nullable();
            $table->string('torihiki_text', 14)->nullable();
            $table->bigInteger('nonyusaki_id')->nullable();
            $table->string('nonyusaki_text', 200)->nullable();
            $table->bigInteger('seikyusaki_id')->nullable();
            $table->string('seikyusaki_text', 200)->nullable();
            $table->string('torihiki_datetime', 8)->nullable();
            $table->string('denpyo_number', 8)->nullable();
            $table->string('biko_text', 300)->nullable();
            $table->integer('order_number')->nullable();
            $table->integer('total_kingaku')->nullable();
            $table->string('created_at', 14)->nullable();
            $table->string('updated_at',14)->nullable();
            $table->bigInteger('updated_user_id')->nullable();
            $table->string('deleted_flg', 1)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nouhinshous');
    }
}
