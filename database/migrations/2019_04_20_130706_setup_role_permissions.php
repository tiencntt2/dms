<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Role;
use App\Models\Permission;
use App\Acl;

class SetupRolePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (Acl::roles() as $role) {
            Role::findOrCreate($role);
        }

        $adminRole = Role::findByName(Acl::ROLE_ADMIN);
        $managerRole = Role::findByName(Acl::ROLE_MANAGER);
        $userRole = Role::findByName(Acl::ROLE_USER);

        foreach (Acl::permissions() as $permission) {
            Permission::findOrCreate($permission, 'api');
        }

        // Setup basic permission
        $adminRole->givePermissionTo([
            Acl::PERMISSION_PERMISSION_MANAGE,
            Acl::PERMISSION_USER_MANAGE,
            Acl::PERMISSION_VIEW_MENU_CUSTOMER,
            Acl::PERMISSION_VIEW_MENU_TANTOU,
            Acl::PERMISSION_CUSTOMER_MANAGE,
            Acl::PERMISSION_TANTOU_MANAGE,
            Acl::PERMISSION_VIEW_MENU_NOUHINSHOUS,
            Acl::PERMISSION_NOUHINSHOUS_MANAGE,
            Acl::PERMISSION_VIEW_MENU_EXECEL,
        ]);
        $managerRole->givePermissionTo([
            Acl::PERMISSION_PERMISSION_MANAGE,
            Acl::PERMISSION_USER_MANAGE,
            Acl::PERMISSION_VIEW_MENU_CUSTOMER,
            Acl::PERMISSION_VIEW_MENU_TANTOU,
            Acl::PERMISSION_CUSTOMER_MANAGE,
            Acl::PERMISSION_TANTOU_MANAGE,
            Acl::PERMISSION_VIEW_MENU_NOUHINSHOUS,
            Acl::PERMISSION_NOUHINSHOUS_MANAGE,
            Acl::PERMISSION_VIEW_MENU_EXECEL,
        ]);
        $userRole->givePermissionTo([
            Acl::PERMISSION_VIEW_MENU_CLIENTS
        ]);

        foreach (Acl::roles() as $role) {
            /** @var \App\User[] $users */
            $users = \App\Models\User::where('role', $role)->get();
            $role = Role::findByName($role);
            foreach ($users as $user) {
                $user->syncRoles($role);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /** @var \App\User[] $users */
        $users = \App\Models\User::all();
        foreach ($users as $user) {
            $roles = array_reverse(Acl::roles());
            foreach ($roles as $role) {
                if ($user->hasRole($role)) {
                    $user->role = $role;
                    $user->save();
                }
            }
        }
    }
}
