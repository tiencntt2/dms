<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->default(1);
            $table->string('code', 10);
            $table->string('login_id', 50)->nullable();
            $table->string('password')->nullable();
            $table->string('login_id2', 50)->nullable();
            $table->string('password2')->nullable();
            $table->string('name', 50);
            $table->string('email', 50)->nullable();
            $table->integer('order')->nullable();
            $table->string('role', 50)->default('user');
            $table->string('created_at', 14)->nullable();
            $table->string('updated_at', 14)->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->string('deleted_flg', 1)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
