<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNouhinshouMeisaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nouhinshou_meisais', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nouhinshou_id');
            $table->string('item_code', 20)->nullable();
            $table->string('item_name', 100)->nullable();
            $table->string('item_class', 100)->nullable();
            $table->string('item_number', 100)->nullable();
            $table->string('item_kikaku', 100)->nullable();
            $table->string('item_quantity', 100)->nullable();
            $table->string('item_tanka', 100)->nullable();
            $table->string('kingaku', 100)->nullable();
            $table->string('zei',100)->nullable();
            $table->string('tekiyo1', 300)->nullable();
            $table->string('tekiyo2', 300)->nullable();
            $table->integer('meisai_line')->nullable();
            $table->integer('order')->default(0);
            $table->string('created_at', 14)->nullable();
            $table->string('updated_at', 14)->nullable();
            $table->bigInteger('updated_user_id')->nullable();
            $table->string('deleted_flg', 1)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nouhinshou_meisais');
    }
}
