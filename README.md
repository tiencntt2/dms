<p align="center">
  <img width="320" src="https://cp5.sgp1.cdn.digitaloceanspaces.com/zoro/laravue-cdn/laravue-logo-line.png">
</p>

# Laravue
[Laravue](https://laravue.dev) (pronounced /ˈlarəvjuː/) is a beautiful dashboard combination of [Laravel](https://laravel.com/), [Vue.js](https://github.com/vuejs/vue) and the UI Toolkit [Element](https://github.com/ElemeFE/element). The work is inspired by  [vue-element-admin](http://panjiachen.github.io/vue-element-admin) with our love on top of that. With the powerful Laravel framework as the backend, Vue.js as the high performance on the frontend,  Laravue appears to be a full-stack solution for an enterprise application level.

Documentation: [https://doc.laravue.dev](https://doc.laravue.dev)
### Installing
#### Manual

```bash
# cp .env.example to .env and config

# Install vendor
composer install

# Migration and DB seeder (after changing your DB settings in .env)
php artisan migrate --seed

# Install dependency with NPM
npm install

# Generate application key
php artisan key:generate

# develop
npm run dev # or npm run watch

# Build on production
npm run production

# Run api server
php artisan serve
```
Open http://localhost:8000 (laravel container port declared in `docker-compose.yml`) to access Laravue

#### Docker
##### Use Laradock
- Document: https://laradock.io/introduction/
- Config project: https://laradock.io/getting-started/
- GitHub: https://github.com/laradock/laradock

```sh
- Clone laradock
- Copy .env.example to .env
- Edit .env
PHP_VERSION=7.4
WORKSPACE_NODE_VERSION=14.17.1
- Enable xdebug
WORKSPACE_INSTALL_XDEBUG=true
PHP_FPM_INSTALL_XDEBUG=true
- Edit .env's project
DB_HOST=mysql
REDIS_HOST=redis
- Run container
docker-compose up -d nginx mysql phpmyadmin redis workspace
- Remove all images	
docker rmi $(docker images -a -q)
- list port
sudo lsof -i -P -n | grep LISTEN
- Reload nginx
docker-compose exec nginx nginx -s reload
```
##### Use Sail Laravel8
```sh
- Install Composer dependencies
composer require laravel/sail --dev
- Create and start container
./vendor/bin/sail up -d
- Alias: Type enter to use sail replace to ./vendor/bin/sail up
alias sail='bash vendor/bin/sail'
1. Stop all container
sail down
2. Run npm
sail npm install
sail npm run dev
3. Run composer
sail composer install
4. Run artisan
sail artisan migrate --seed
5. sail help
Type sail then enter
6. Hot Autoload
npm run hot (sail npm run hot not active on browers)
```

## Running the tests
* Tests system is under development

## Deployment and/or CI/CD
This project uses [Envoy](https://laravel.com/docs/5.8/envoy) for deployment, and [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/). Please check `Envoy.blade.php` and `.gitlab-ci.yml` for more detail.

## Built with
* [Laravel](https://laravel.com/) - The PHP Framework For Web Artisans
* [Laravel Sanctum](https://github.com/laravel/sanctum/) - Laravel Sanctum provides a featherweight authentication system for SPAs and simple APIs.
* [spatie/laravel-permission](https://github.com/spatie/laravel-permission) - Associate users with permissions and roles.
* [VueJS](https://vuejs.org/) - The Progressive JavaScript Framework
* [Element](https://element.eleme.io/) - A  Vue 2.0 based component library for developers, designers and product managers
* [Vue Admin Template](https://github.com/PanJiaChen/vue-admin-template) - A minimal vue admin template with Element UI
* L5-Swagger (Laravel Api Document Generator)
  - GitHub:
  https://github.com/DarkaOnLine/L5-Swagger
  - Reference add comment at here:
  https://drive.google.com/file/d/1bipU1pbhxsSZfbFR626-9zpYtDLnoB37/view?usp=sharing
  - Document:(replace @SWG = @OA)
  https://viblo.asia/p/l5-swagger-in-laravel-rest-apis-m68Z0x1AZkG

## Two link admin
- For Admin + Manager: /login
- For Client: /client/login

## Related projects

* [Laravue-core](https://github.com/tuandm/laravue-core) - Laravel package which provides core functionalities of Laravue.

## Acknowledgements

* [vue-element-admin](https://panjiachen.github.io/vue-element-admin/#/) A magical vue admin which insprited Laravue project.
* [tui.editor](https://github.com/nhnent/tui.editor) - Markdown WYSIWYG Editor.
* [Echarts](http://echarts.apache.org/) - A powerful, interactive charting and visualization library for browser.
