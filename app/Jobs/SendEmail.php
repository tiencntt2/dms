<?php

namespace App\Jobs;

use App\Models\ImportLog;
use App\Models\User;
use App\Repositories\Eloquent\ImportLogRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    private $log;

    /**
     * Create a new job instance.
     *
     * @param $data
     * @param $log
     */
    public function __construct($data, $log)
    {
        $this->data = $data;
        $this->log = $log;
    }

    /**
     * Execute the job.
     *
     * @param ImportLogRepository $importLog
     * @return void
     */
    public function handle(ImportLogRepository $importLog)
    {
        $user = User::find($this->log->user_id);

        Mail::to($user->email)->send(new MailNotify($this->data));

        $importLog->update($this->log->id, ['status' => ImportLog::COMPLETED]);
    }
}
