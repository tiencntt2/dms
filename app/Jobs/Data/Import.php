<?php

namespace App\Jobs\Data;

use App\Models\User;
use App\Repositories\Eloquent\ClientRepository;
use App\Repositories\Eloquent\NouhinshouMeisaiRepository;
use App\Repositories\Eloquent\NouhinshouRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Import implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $row;
    private $type;

    const FIELD_CONDITION = '0';
    const GROUP_CONDITION = '6';
    const MEISAI_LINE = 90;

    /**
     * Create a new job instance.
     *
     * @param $row
     * @param $type
     */
    public function __construct($row, $type)
    {
        $this->row = $row;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @param NouhinshouRepository $nouhinshouRepo
     * @param NouhinshouMeisaiRepository $nouhinshouMeisaiRepo
     * @param ClientRepository $clientRepo
     * @return void
     */
    public function handle(
        NouhinshouRepository $nouhinshouRepo,
        NouhinshouMeisaiRepository $nouhinshouMeisaiRepo,
        ClientRepository $clientRepo
    )
    {
        \DB::beginTransaction();

        try {
            $denpyo_number = ($this->type == 1) ? $this->row[6] : $this->row[8];

            if (!$denpyo_number) return;

            $nouhinshou = $nouhinshouRepo->findByField('denpyo_number', $denpyo_number)->last();

            if (!$nouhinshou) {
                $seikyusaki_code = $this->row[0];
                $seikyusaki = $clientRepo->findByField('code', $seikyusaki_code)->first();
                $nonyusaki_code = $this->row[2];
                $nonyusaki = $clientRepo->findByField('code', $nonyusaki_code)->first();
                $user_code = $this->row[4];
                $user = User::where('code', $user_code)->first();

                $attributes = [
                    'seikyusaki_id' => ($seikyusaki) ? $seikyusaki->id : null,
                    'seikyusaki_text' => $this->row[1],
                    'nonyusaki_id' => ($nonyusaki) ? $nonyusaki->id : null,
                    'nonyusaki_text' => $this->row[3],
                    'tantou_id' => ($user) ? $user->id : null,
                    'denpyo_number' => $denpyo_number,
                    'torihiki_code' => ($this->type == 1) ? $this->row[9] : $this->row[5],
                    'torihiki_text' => ($this->type == 1) ? $this->row[10] : $this->row[6],
                    'torihiki_datetime' => ($this->type == 1) ? $this->row[11] : $this->row[7],
                    'biko_text' => ($this->type == 1) ? ($this->row[24] ?? null) : ($this->row[19] ?? null),
                ];

                $nouhinshou = $nouhinshouRepo->create($attributes);
            }

            $combineNouhinshouMeisai = $nouhinshouMeisaiRepo->findWhere([
                'nouhinshou_id' => $nouhinshou->id,
                'meisai_line' => self::MEISAI_LINE
            ])->first();

            if (!$combineNouhinshouMeisai) {
                $meisaiAttributes = [
                    'meisai_line' => ($this->type == 1) ? $this->row[7] : $this->row[9],
                    'item_name' => ($this->type == 1) ? $this->row[12] : $this->row[11],
                    'tekiyo1' => ($this->type == 1) ? $this->row[14] : $this->row[17],
                    'item_code' => ($this->type == 1) ? $this->row[16] : $this->row[10],
                    'item_quantity' => ($this->type == 1) ? $this->row[19] : $this->row[13],
                    'item_tanka' => ($this->type == 1) ? $this->row[20] : $this->row[14],
                    'kingaku' => ($this->type == 1) ? $this->row[21] : $this->row[15],
                    'zei' => ($this->type == 1) ? ($this->row[22] ?? null) : $this->row[16],
                    'tekiyo2' => ($this->type == 1) ? ($this->row[23] ?? null) : ($this->row[18] ?? null),
                ];

                if ($this->type == 1) {
                    $meisaiAttributes['item_class'] = $this->row[13];
                    $meisaiAttributes['item_number'] = $this->row[17];
                } else{
                    $meisaiAttributes['item_kikaku'] = $this->row[12];
                }

                $meisaiAttributes['nouhinshou_id'] = $nouhinshou->id;

                // create nouhinshouMeisai
                $nouhinshouMeisaiRepo->create($meisaiAttributes);
            }
        }catch (\Exception $e) {
            \Log::error($e);
            \DB::rollback();
        }

        \DB::commit();
    }
}
