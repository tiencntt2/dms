<?php

namespace App\Imports;

use App\Jobs\Data\Import;
use App\Jobs\SendEmail;
use App\Models\ImportLog;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Contracts\Queue\ShouldQueue;

class DataImport implements ToCollection, WithBatchInserts, ShouldQueue, WithChunkReading, WithStartRow
{
    private $log;
    private $type;

    public function __construct($log, $type){
        $this->log = $log;
        $this->type = $type;
    }

    const IMPORT_TYPE_1 = 1;
    const IMPORT_TYPE_2 = 2;

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        $groups = $rows->where(Import::FIELD_CONDITION, '!=', null)->groupBy(Import::GROUP_CONDITION);
        $count = $groups->count();
        $rows = $rows->where(Import::FIELD_CONDITION, '!=', null);
        $num_row_import = $rows->count();

        // create import log
        $this->log->update(['total_record' => $count]);

        $i = 0;

        foreach ($rows as $key => $row) {
            $rowJob = (new Import($row, $this->type))->delay(Carbon::now()->addSeconds(3));
            dispatch($rowJob);

            $i++;

            if ($i == $num_row_import) {
                $message = [
                    'type' => 'Import  Finish',
                    'task' => 'Import file: '. $this->log->file_name,
                    'content' => 'done!',
                ];

                SendEmail::dispatch($message, $this->log)->delay(now()->addMinute(1));
            }
        }
    }

    public function startRow(): int
    {
        return 1;
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
