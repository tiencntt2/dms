<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nouhinshous extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nouhinshous';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tantou_id',
        'torihiki_code',
        'torihiki_text',
        'nonyusaki_id',
        'nonyusaki_text',
        'seikyusaki_id',
        'seikyusaki_text',
        'torihiki_datetime',
        'denpyo_number',
        'biko_text',
        'order_number',
        'total_kingaku',
        'updated_user_id',
        'deleted_flg',
        'created_at',
        'updated_at'
    ];

    const NOT_DELETE = 0;
    const DELETETED = 1;

    /**
     * @param $value
     */
    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = preg_replace('/[-: ]/', null, $value);
    }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value)
    {
        $this->attributes['updated_at'] = preg_replace('/[-: ]/', null, $value);
    }

    /**
     * @param $value
     */
    public function setTorihikiDatetimeAttribute($value)
    {
        $this->attributes['torihiki_datetime'] = preg_replace('/[-: ]/', null, $value);
    }

    public function nouhinshouMeisais()
    {
        return $this->hasMany(NouhinshouMeisai::class, 'nouhinshou_id');
    }
}
