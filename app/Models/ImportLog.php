<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportLog extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'import_logs';

    const NOT_DELETE = 0;
    const PROCESSING = 1;
    const COMPLETED = 2;
    const ERROR = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'total_record',
        'start_number',
        'end_number',
        'status',
        'user_id',
        'process_time'
    ];
}
