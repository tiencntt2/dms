<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NouhinshouMeisai extends Model
{
    use HasFactory;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nouhinshou_meisais';

    /**
     * Set permissions guard to API by default
     * @var string
     */
    protected $guard_name = 'api';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nouhinshou_id',
        'item_code',
        'item_name',
        'item_class',
        'item_number',
        'item_kikaku',
        'item_quantity',
        'item_tanka',
        'kingaku',
        'zei',
        'tekiyo1',
        'tekiyo2',
        'meisai_line',
        'order',
        'updated_user_id',
        'deleted_flg',
        'created_at',
        'updated_at'
    ];

    const NOT_DELETE = 0;
    const DELETETED = 1;
    const COMBINE = 90;

    /**
     * @param $value
     */
    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = preg_replace('/[-: ]/', null, $value);
    }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value)
    {
        $this->attributes['updated_at'] = preg_replace('/[-: ]/', null, $value);
    }
}
