<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property Role[] $roles
 *
 * @method static User create(array $user)
 * @package App
 */
class Client extends Authenticatable
{
    use Notifiable, HasRoles, HasApiTokens;

    protected $guard = 'client';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login_id', 'password', 'login_id2', 'password2', 'code', 'email', 'postal_code', 'address', 'order', 'role', 'type', 'updated_user', 'deleted_flg', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'password2'];

    /**
     * Set permissions guard to API by default
     * @var string
     */
    protected $guard_name = 'api';

    const NOT_DELETE = 0;
    const DELETED = 1;
    const TYPE_SEIKYUSAKI = 1;
    const TYPE_NONYUSAKI = 2;
    const MODEL_HAS_ROLE = 3;
    const PASSWORD_DEFAULT = '@@@@@@';
    /**
     * @param $value
     */
    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = preg_replace('/[-: ]/', null, $value);
    }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value)
    {
        $this->attributes['updated_at'] = preg_replace('/[-: ]/', null, $value);
    }

    /**
     * Check if user has a permission
     * @param String
     * @return bool
     */
    public function hasPermission($permission): bool
    {
        foreach ($this->roles as $role) {
            if (in_array($permission, $role->permissions->pluck('name')->toArray())) {
                return true;
            }
        }
        return false;
    }

    public static function getCode($id) {
        return Client::select('code')->where('id', $id)->first();

    }
}
