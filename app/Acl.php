<?php
/**
 * File Acl.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Class Acl
 *
 * @package App
 */
final class Acl
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_USER = 'user';

    const PERMISSION_VIEW_MENU_CLIENTS = 'view menu client';
    const PERMISSION_VIEW_MENU_NOUHINSHOUS = 'view menu nouhinshous';
    const PERMISSION_VIEW_MENU_CUSTOMER = 'view menu customer';
    const PERMISSION_VIEW_MENU_TANTOU = 'view menu tantou';
    const PERMISSION_VIEW_MENU_EXECEL = 'view menu excel';

    const PERMISSION_USER_MANAGE = 'manage user';
    const PERMISSION_PERMISSION_MANAGE = 'manage permission';
    const PERMISSION_NOUHINSHOUS_MANAGE = 'manage nouhinshous';
    const PERMISSION_CUSTOMER_MANAGE = 'manage customer';
    const PERMISSION_TANTOU_MANAGE = 'manage tantou';

    /**
     * @param array $exclusives Exclude some permissions from the list
     * @return array
     */
    public static function permissions(array $exclusives = []): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function($value, $key) use ($exclusives) {
                return !in_array($value, $exclusives) && Str::startsWith($key, 'PERMISSION_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }

    public static function menuPermissions(): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function($value, $key) {
                return Str::startsWith($key, 'PERMISSION_VIEW_MENU_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }

    /**
     * @return array
     */
    public static function roles(): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $roles =  Arr::where($constants, function($value, $key) {
                return Str::startsWith($key, 'ROLE_');
            });

            return array_values($roles);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }
}
