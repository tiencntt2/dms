<?php
/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\JsonResponse;
use App\Mail\AdminResetPassword;
use App\Mail\ClientResetPassword;
use App\Repositories\Eloquent\CustomRepository;
use App\Repositories\Eloquent\TantouRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Api
 */
class AuthController extends BaseController
{
    protected $tantou;
    protected $custom;

    public function __construct(TantouRepository $tantou, CustomRepository $custom) {
        $this->tantou = $tantou;
        $this->custom = $custom;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('login_id', 'password');
        $credentials['deleted_flg'] = 0;

        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'ログインIDまたはパスワードに間違いがあります。'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();

        return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function loginClient(Request $request)
    {
        $credentials = $request->only('login_id', 'password');
        $credentials['deleted_flg'] = 0;

        if (!Auth::guard('client')->attempt($credentials)) {
            return response()->json(new JsonResponse([], 'ログインIDまたはパスワードに間違いがあります。'), Response::HTTP_UNAUTHORIZED);
        }

        $client = auth()->guard('client')->user();

        return response()->json(new JsonResponse(new UserResource($client)), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logoutClient(Request $request)
    {
        Auth::guard('client')->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function resetPassword(Request $request)
    {
        if (isset($request->login_id) && !empty($request->login_id)) {
            $checkWithLoginID = $this->tantou->findLoginID($request->login_id);
            if (!$checkWithLoginID) return response()->json(['error' => '入力したログインIDが存在しません。'], 404);
        }

        if (isset($request->email) && !empty($request->email)) {
            $checkWithEmail = $this->tantou->findEmail($request->email);
            if (!$checkWithEmail) return response()->json(['error' => '入力したメールが存在しません。'], 404);
        }

        $tantou = $this->tantou->findWithLoginIDAndEmail($request->login_id, $request->email);

        if (!$tantou) return response()->json(['error' => 'ログインIDまたはメールアドレスに間違いがあります。'], 404);

        $newPasswordString = $this->_generatePasswordString();
        $newPassword = Hash::make($newPasswordString);

        $tantou->update(['password' => $newPassword]);

        $data = [
            'name' => $tantou->name,
            'password' => $newPasswordString
        ];

        try {
            Mail::to($tantou->email)->send(new AdminResetPassword($data));
            return response()->json(['success' => 'パスワードがリセットしました。メールを確認してください。'], 200);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    public function clientResetPassword(Request $request)
    {
        if (isset($request->login_id) && !empty($request->login_id)) {
            $checkWithLoginID = $this->custom->findLoginID($request->login_id);
            if (!$checkWithLoginID) return response()->json(['error' => '入力したログインIDが存在しません。'], 404);
        }

        if (isset($request->email) && !empty($request->email)) {
            $checkWithEmail = $this->custom->findEmail($request->email);
            if (!$checkWithEmail) return response()->json(['error' => '入力したメールが存在しません。'], 404);
        }

        $custom = $this->custom->findWithLoginIDAndEmail($request->login_id, $request->email);

        if (!$custom) return response()->json(['error' => 'ログインIDまたはメールアドレスに間違いがあります。'], 404);

        $newPasswordString = $this->_generatePasswordString();
        $newPassword = Hash::make($newPasswordString);

        $custom->update(['password' => $newPassword]);

        $data = [
            'name' => $custom->name,
            'password' => $newPasswordString
        ];

        try {
            Mail::to($custom->email)->send(new ClientResetPassword($data));
            return response()->json(['success' => 'パスワードがリセットしました。メールを確認してください。'], 200);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    private function _generatePasswordString($length = 6)
    {
        $template = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($template, ceil($length / strlen($template)))), 1, $length);
    }
}
