<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ImportLogResource;
use App\Imports\DataImport;
use App\Models\ImportLog;
use App\Repositories\Eloquent\ImportLogRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    protected $importLogRepo;

    public function __construct(ImportLogRepository $importLogRepo){
        $this->importLogRepo = $importLogRepo;
    }

    /**
     * import csv with type 1
     * @param Request $request
     */
    public function importCsvType1(Request $request)
    {
        $this->_importCsv($request, DataImport::IMPORT_TYPE_1);
    }

    /**
     * import csv with type 2
     * @param Request $request
     */
    public function importCsvType2(Request $request)
    {
        $this->_importCsv($request, DataImport::IMPORT_TYPE_2);
    }

    private function _importCsv(Request $request, $type)
    {
        $log = null;

        try {
            $file_name = $request->file('import_file')->getClientOriginalName();
            // create import log
            $log = $this->importLogRepo->create([
                'file_name' => $file_name,
                'total_record' => 0,
                'user_id' => auth()->user()->id,
                'status' => ImportLog::PROCESSING,
            ]);

            Excel::import(new DataImport($log, $type), $request->file('import_file'));

            return response()->json([
                'status' => 'success',
            ], 200);
        } catch (\Exception $exception) {
            $this->importLogRepo->update($log->id, ['status' => ImportLog::ERROR]);
            return response()->json([
                'status' => 'false',
            ], 403);
        }

    }

    public function removeLog($id)
    {
        $this->importLogRepo->destroy($id);

        return response()->json('successfully deleted');
    }

    public function getListImportLogs(Request $request)
    {
        return ImportLogResource::collection(
            $this->importLogRepo->apiPaginateBy('id', 'asc', $request->limit ?: 10, ['import_logs.*'])
        );
    }
}
