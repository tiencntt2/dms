<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NouhinshouCSVResource;
use App\Http\Resources\NouhinshouCSVResource2;
use App\Http\Resources\NouhinshouDetailResource;
use App\Http\Resources\NouhinshouResource;
use App\Models\Client;
use App\Models\User;
use App\Repositories\Eloquent\ClientRepository;
use App\Repositories\Eloquent\NouhinshouMeisaiRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\Eloquent\NouhinshouRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class NouhinshouController extends Controller
{

    protected $nouhinshouRepo;
    protected $nouhinshouMeisaiRepo;
    private $clientRepository;

    public function __construct(
        NouhinshouRepository $nouhinshou,
        NouhinshouMeisaiRepository $nouhinshouMeisaiRepo,
        ClientRepository $clientRepository
    )
    {
        $this->nouhinshouRepo = $nouhinshou;
        $this->nouhinshouMeisaiRepo = $nouhinshouMeisaiRepo;
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $torihiki_datetime = [
            'date_from' => $request->torihiki_datetime ? preg_replace('/[-: ]/', null, $request->torihiki_datetime[0]) : null,
            'date_to' => $request->torihiki_datetime ? preg_replace('/[-: ]/', null, $request->torihiki_datetime[1]) : null,
        ];

        $client_type = $request->client_id ? $this->clientRepository->find($request->client_id)->type : '';
        return NouhinshouResource::collection($this->nouhinshouRepo->apiPaginateBy(
            $request->role ?: '',
            $request->client_id ?: '',
            $client_type,
            $torihiki_datetime,
            [
                'denpyo_number' => $request->denpyo_number,
                'nonyusaki_id' => $request->nonyusaki_id,
                'seikyusaki_id' => $request->seikyusaki_id,
                'tantou_id' => $request->tantou_id,
                'torihiki_code' => $request->torihiki_code,
                'torihiki_text' => $request->torihiki_text,
                'biko_text' => $request->biko_text,
            ],
            $request->orderBy ?: 'id',
            $request->order ?: 'asc',
            $request->limit ?: 50,
            ['nouhinshous.*']
        ));
    }

    public function listCSVType1(Request $request)
    {
        $list_array = $this->getArrayId($request->all());
        $list_nouhinshou = $this->sortListCSV($this->nouhinshouRepo->listNouhinshouCSVBy($list_array)->toArray());
        return NouhinshouCSVResource::collection($list_nouhinshou);
    }

    public function listCSVType2(Request $request)
    {
        $list_array = $this->getArrayId($request->all());
        $list_nouhinshou = $this->sortListCSV($this->nouhinshouRepo->listNouhinshouCSVBy($list_array)->toArray());
        return NouhinshouCSVResource2::collection($list_nouhinshou);
    }

    public function getCSVs(Request $request) {
        $client_id = $request->client_id ? $request->client_id : '';
        $torihiki_datetime = [
            'date_from' => $request->torihiki_datetime ? preg_replace('/[-: ]/', null, $request->torihiki_datetime[0]).'000000' : null,
            'date_to' => $request->torihiki_datetime ? preg_replace('/[-: ]/', null, $request->torihiki_datetime[1]).'235959': null,
        ];
        $attribute_search = [
            'denpyo_number' => $request->denpyo_number,
            'torihiki_text' => $request->torihiki_text,
            'biko_text' => $request->biko_text,
        ];
        return $this->nouhinshouRepo->listCSVs($client_id, $torihiki_datetime, $attribute_search)->toArray();
    }

    private function getArrayId($arrays) {
        $array_id = array();
        foreach ($arrays as $key => $value) {
            array_push($array_id, $value[0]);
        }
        return $array_id;
    }

    private function sortListCSV ($list_nouhinshou) {
        $array_nouhinshou = [];
        foreach ($list_nouhinshou as $value) {
            if(count($value['nouhinshou_meisais']) > 1) {
                foreach ($value['nouhinshou_meisais'] as $nouhinshou_meisai){
                    $attribute = [
                        "id" => $value['id'],
                        "tantou_id" => $value['tantou_id'],
                        "torihiki_code" => $value['torihiki_code'],
                        "torihiki_text" => $value['torihiki_text'],
                        "nonyusaki_id" => $value['nonyusaki_id'],
                        "nonyusaki_text" => $value['nonyusaki_text'],
                        "seikyusaki_id" => $value['seikyusaki_id'],
                        "seikyusaki_text" => $value['seikyusaki_text'],
                        "torihiki_datetime" => $value['torihiki_datetime'],
                        "denpyo_number" => $value['denpyo_number'],
                        "biko_text" => $value['biko_text'],
                        "order_number" => $value['order_number'],
                        "total_kingaku" => $value['total_kingaku'],
                        "created_at" => $value['created_at'],
                        "updated_at" => $value['updated_at'],
                        "updated_user_id" => $value['updated_user_id'],
                        "deleted_flg" => $value['deleted_flg'],
                        "nouhinshou_meisais" => [
                            "id" => $nouhinshou_meisai['id'],
                            "nouhinshou_id" =>$nouhinshou_meisai['nouhinshou_id'],
                            "item_code" => $nouhinshou_meisai['item_code'],
                            "item_name" => $nouhinshou_meisai['item_name'],
                            "item_class" => $nouhinshou_meisai['item_class'],
                            "item_number" => $nouhinshou_meisai['item_number'],
                            "item_kikaku" => $nouhinshou_meisai['item_kikaku'],
                            "item_quantity" => $nouhinshou_meisai['item_quantity'],
                            "item_tanka" => $nouhinshou_meisai['item_tanka'],
                            "kingaku" => $nouhinshou_meisai['kingaku'],
                            "zei" => $nouhinshou_meisai['zei'],
                            "tekiyo1" => $nouhinshou_meisai['tekiyo1'],
                            "tekiyo2" => $nouhinshou_meisai['tekiyo2'],
                            "meisai_line" => $nouhinshou_meisai['meisai_line'],
                            "order" => $nouhinshou_meisai['order'],
                            "created_at" => $nouhinshou_meisai['created_at'],
                            "updated_at" => $nouhinshou_meisai['updated_at'],
                            "updated_user_id" => $nouhinshou_meisai['updated_user_id'],
                            "deleted_flg" => $nouhinshou_meisai['deleted_flg'],
                        ]
                    ];
                    array_push($array_nouhinshou, $attribute);
                }

            } else {
                array_push($array_nouhinshou, $value);
            }
        }
        return $array_nouhinshou;
    }

    public function pdfView1($id){
        $nouhinshou = $this->nouhinshouRepo->getNouhinshou($id)->toArray();

        $nonyusaki_code = Client::select('code')->where('id', $nouhinshou['nonyusaki_id'])->first();
        $tantou_code = User::select('code')->where('id', $nouhinshou['tantou_id'])->first();
        $nonyusaki_name = Client::select('name')->where('id', $nouhinshou['nonyusaki_id'])->first();
        $nouhinshou['nonyusaki_code'] = isset($nonyusaki_code['code']) ? $nonyusaki_code['code'] : '';
        $nouhinshou['tantou_code'] = isset($tantou_code['code']) ? $tantou_code['code'] : '';
        $nouhinshou['nonyusaki_name'] = isset($nonyusaki_name['name']) ? $nonyusaki_name['name'] : '';

        $date = new Carbon( $nouhinshou['torihiki_datetime'] );
        $nouhinshou['year'] = $date->year;
        $nouhinshou['month'] = $date->month;
        $nouhinshou['day'] = $date->day;

        if (count($nouhinshou['nouhinshou_meisais']) > 1) {
            $nouhinshou = $this->sortNouhinshou($nouhinshou);
        }

        return [
            'view' => view('pdf.pdf_view1', ['nouhinshou' => $nouhinshou])->render(),
            'denpyo_number' => $nouhinshou['denpyo_number']
        ];
    }

    public function pdfView2($id)
    {
        $nouhinshou = $this->nouhinshouRepo->getNouhinshou($id);
        $nonyusaki = $this->clientRepository->find($nouhinshou->nonyusaki_id);
        $seikyusaki = $this->clientRepository->find($nouhinshou->seikyusaki_id);
        $tantou = User::find($nouhinshou->tantou_id);
        $nouhinshou->created_at = date('Y/m/d', strtotime($nouhinshou->created_at));
        $nouhinshou->torihiki_datetime = Carbon::parse($nouhinshou->torihiki_datetime)->format('Y/m/d');
        $nouhinshou->tantou_code = $tantou->code ?? null;
        $nouhinshou->nonyusaki_code = $nonyusaki->code ?? null;
        $nouhinshou->nonyusaki_name = $nonyusaki->name ?? null;
        $nouhinshou->seikyusaki_name = $seikyusaki->name ?? null;
        $result = $nouhinshou->toArray();
        if (count($result["nouhinshou_meisais"]) > 1) {
            $result = $this->sortNouhinshou($result);
        }

        return view('pdf.pdf2', ['nouhinshou' => $result])->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return NouhinshouResource|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (!$request->denpyo_number) return response()->json(['error' => '納品書情報の新規登録が失敗しました。'], 403);

        /* TODO: thay doi message cho bug DMS_202106-60 */
//        $nouhinshou = $this->nouhinshouRepo->findByField('denpyo_number', $request->denpyo_number)->last();
//
//        if (!$nouhinshou) return response()->json(['error' => '納品書情報の新規登録が失敗しました。'], 403);

        $attributes = [
            'denpyo_number' => $request->denpyo_number,
            'torihiki_text' => $request->torihiki_text,
            'torihiki_code' => $request->torihiki_code,
            'nonyusaki_id' => $request->nonyusaki_id,
            'seikyusaki_id' => $request->seikyusaki_id,
            'tantou_id' => $request->tantou_id,
            'torihiki_datetime' => Carbon::parse($request->torihiki_datetime)->format('Ymd'),
            'biko_text' => $request->biko_text,
            'created_at' => Carbon::now()->format('Ymdhis'),
            'updated_at' => Carbon::now()->format('Ymdhis'),
        ];

        $meisaiAttributes = $request->nouhinshou_meisais;

        \DB::beginTransaction();

        try {
            // create Nouhinshou
            $nouhinshou = $this->nouhinshouRepo->create($attributes);

            // create or update nouhinshouMeisai
            if ($nouhinshou) {
                $last = $meisaiAttributes[count($meisaiAttributes) - 1];

                if ($last['item_quantity'] && $last['kingaku'] && $last['zei']) {
                    $this->_createNouhinshouMeisai($meisaiAttributes, $nouhinshou);
                }
            }

            \DB::commit();

            return new NouhinshouResource($nouhinshou);
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return response()->json(['error' => '納品書情報の新規登録が失敗しました。'], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return NouhinshouDetailResource
     */
    public function show($id)
    {
        return new NouhinshouDetailResource($this->nouhinshouRepo->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return NouhinshouResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $id = $request->id;

        $nouhinshou = $this->nouhinshouRepo->find($id);

        if (!$nouhinshou) return response()->json(['error' => '納品書情報の変更が失敗しました。'], 404);

        $attributes = [
            'denpyo_number' => $request->denpyo_number,
            'torihiki_text' => $request->torihiki_text,
            'torihiki_code' => $request->torihiki_code,
            'nonyusaki_id' => $request->nonyusaki_id,
            'seikyusaki_id' => $request->seikyusaki_id,
            'tantou_id' => $request->tantou_id,
            'torihiki_datetime' => Carbon::parse($request->torihiki_datetime)->format('Ymd'),
            'biko_text' => $request->biko_text,
            'updated_at' => Carbon::now()->format('Ymdhis'),
        ];

        $meisaiAttributes = $request->nouhinshou_meisais;

        \DB::beginTransaction();

        try {
            $this->nouhinshouRepo->update($id, $attributes);

            // create or update nouhinshouMeisai
            $last = $meisaiAttributes[count($meisaiAttributes) - 1];

            if ($last['item_quantity'] && $last['kingaku'] && $last['zei']) {
                $this->_createNouhinshouMeisai($meisaiAttributes, $nouhinshou);
            } else{
                return response()->json(['error' => ''], 403);
            }

            \DB::commit();

            return new NouhinshouResource($nouhinshou);
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return response()->json(['error' => '納品書情報の変更が失敗しました。'], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->nouhinshouRepo->destroy($id);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }

        return response()->json(null, 204);
    }

    private function _createNouhinshouMeisai($meisaiAttributes, $nouhinshou)
    {
        foreach ($meisaiAttributes as $meisaiAttribute) {
            if ($meisaiAttribute['item_quantity']) {
                $meisaiAttribute['nouhinshou_id'] = $nouhinshou->id;

                if (array_key_exists('zei', $meisaiAttribute) && $meisaiAttribute['zei']) {
                    $meisaiAttribute['meisai_line'] = 90;
                } else {
                    $meisaiAttribute['meisai_line'] = $meisaiAttribute['index'];
                }

                $meisaiAttribute['updated_at'] = Carbon::now()->format('Ymdhis');
                // update nouhinshouMeisai
                if (array_key_exists('id', $meisaiAttribute) && $meisaiAttribute['id']) {
                    $this->nouhinshouMeisaiRepo->update($meisaiAttribute['id'], $meisaiAttribute);
                }else{
                    // create nouhinshouMeisai
                    $meisaiAttribute['created_at'] = Carbon::now()->format('Ymdhis');
                    $this->nouhinshouMeisaiRepo->create($meisaiAttribute);
                }
            }
        }
    }

    private function sortNouhinshou($nouhinshou)
    {
        $first = $second = [];
        foreach ($nouhinshou["nouhinshou_meisais"] as $index => $nouhinshou_meisai) {
            if ($nouhinshou_meisai['meisai_line'] == 90) {
                $nouhinshou["nouhinshou_meisais"][$index]['total'] = $nouhinshou["nouhinshou_meisais"][$index]['kingaku'] +
                    $nouhinshou["nouhinshou_meisais"][$index]['zei'];
                $second[] = $nouhinshou["nouhinshou_meisais"][$index];
            } else {
                $first[] = $nouhinshou_meisai;
            }
        }
        $nouhinshou["nouhinshou_meisais"] = array_merge($first, $second);

        return $nouhinshou;
    }
}
