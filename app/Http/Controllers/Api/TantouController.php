<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CustomCollection;
use App\Http\Resources\TantouResource;
use App\Models\Client;
use App\Models\User;
use App\Repositories\Eloquent\CustomRepository;
use App\Repositories\Eloquent\TantouRepository;
use Illuminate\Http\Request;

class TantouController extends BaseController
{
    protected $tantou;
    public function __construct(TantouRepository $tantou) {
        $this->tantou = $tantou;
    }
    /**
     * Display a listing of the resource.
     *
     * @return CustomCollection
     */
    public function index(Request $request)
    {
        $tantous = $this->tantou->apiListTantou($request->all());
        return TantouResource::collection($tantous);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tantou = $this->saveSession($request->all());
        if (isset($request->login_id) && !empty($request->login_id)) {
            $tan = $this->tantou->findLoginID($request->login_id);
            if(isset($tan->login_id) && $request->login_id == $tan->login_id){
                return response()->json(['status' => 'true']);
            }
        }
        return response()->json($tantou);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $tantou = $this->tantou->apiStoreTantou($request->all());
        session()->forget('tantou');
        return response()->json($tantou);
    }

    private function saveSession($param){
        session(['tantou.code' => $param['code']]);
        session(['tantou.name' => $param['name']]);
        session(['tantou.email' => $param['email']]);
        session(['tantou.login_id' => $param['login_id']]);
        session(['tantou.password' => isset($param['password']) ? $param['password'] : '']);
        return session('tantou');
    }

    public function removeSession(){
        session()->forget('tantou');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $session = $this->saveSession($request->all());
        if (isset($request->login_id) && !empty($request->login_id)) {
            $tan = $this->tantou->find($id);
            if(isset($tan->login_id) && $request->login_id == $tan->login_id){
                return response()->json(['status' => 'false']);
            } elseif (isset($tan->login_id) && $request->login_id != $tan->login_id) {
                $tantou = $this->tantou->findLoginID($request->login_id);
                if(isset($tantou->login_id) && $request->login_id == $tantou->login_id){
                    return response()->json(['status' => 'true']);
                }
            }
        }
        return response()->json($session);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $tantou = session('tantou');
        return response()->json($tantou);
    }

    public function getEdit($id)
    {
        if (session()->exists('tantou')) {
            return response()->json(session('tantou'));
        }
        $tantou = User::find($id);
        return response()->json($tantou);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $tantou = $this->tantou->apiUpdateTantou($id, $request->all());
        session()->forget('tantou');
        return response()->json('successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->tantou->destroy($id);

        return response()->json('successfully deleted');
    }
}
