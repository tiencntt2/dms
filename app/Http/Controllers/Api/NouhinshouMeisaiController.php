<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\NouhinshouMeisaiRepository;
use Illuminate\Http\Request;

class NouhinshouMeisaiController extends Controller
{

    protected $nouhinshouMeisai;

    public function __construct(NouhinshouMeisaiRepository $nouhinshouMeisai)
    {
        $this->nouhinshouMeisai = $nouhinshouMeisai;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $nouhinshouMesai = $this->nouhinshouMeisai->find($id);
            $nouhinshouId = $nouhinshouMesai->nouhinshou_id;

            if (!$nouhinshouMesai) return response()->json(null, 204);

            $this->nouhinshouMeisai->destroy($id);
            // re-caculate nouhinshoumeisai with meisai_line = 90
            $this->nouhinshouMeisai->updateNouhinshouMeisaiCombine($nouhinshouId);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 403);
        }

        return response()->json(null, 204);
    }
}
