<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CustomCollection;
use App\Http\Resources\CustomResource;
use App\Models\Client;
use App\Repositories\Eloquent\CustomRepository;
use Illuminate\Http\Request;

class CustomController extends BaseController
{
    protected $customer;
    public function __construct(CustomRepository $customer) {
        $this->customer = $customer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $customers = $this->customer->apiListCustomer($request->all());
        return CustomResource::collection($customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $custom = $this->saveSession($request->all());
        if (isset($request->login_id) && !empty($request->login_id)) {
            $cus = $this->customer->findLoginID($request->login_id);
            if(isset($cus->login_id) && $request->login_id == $cus->login_id){
                return response()->json(['status' => 'true']);
            }
        }
        return response()->json($custom);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $customer = $this->customer->apiStoreCustomer($request->all());
        session()->forget('custom');
        return response()->json($customer);
    }

    private function saveSession($param){
        session(['custom.type' => $param['type']]);
        session(['custom.code' => $param['code']]);
        session(['custom.name' => $param['name']]);
        session(['custom.email' => $param['email']]);
        session(['custom.postal_code' => $param['postal_code']]);
        session(['custom.address' => $param['address']]);
        session(['custom.login_id' => $param['login_id']]);
        session(['custom.password' => isset($param['password']) ? $param['password'] : '']);
        session(['custom.login_id2' => $param['login_id2']]);
        session(['custom.password2' => isset($param['password2']) ? $param['password2'] : '']);
        return session('tantou');
    }

    public function removeSession(){
        session()->forget('custom');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, Request $request)
    {
        $session = $this->saveSession($request->all());
        if (isset($request->login_id) && !empty($request->login_id)) {
            $cus = $this->customer->find($id);
            if(isset($cus->login_id) && $request->login_id == $cus->login_id){
                return response()->json(['status' => 'false']);
            } elseif (isset($tan->login_id) && $request->login_id != $tan->login_id) {
                $custom = $this->customer->findLoginID($request->login_id);
                if(isset($custom->login_id) && $request->login_id == $custom->login_id){
                    return response()->json(['status' => 'true']);
                }
            }
        }
        return response()->json($session);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $tantou = session('custom');
        return response()->json($tantou);
    }

    public function getEdit($id)
    {
        if (session()->exists('custom')) {
            return response()->json(session('custom'));
        }
        $customer = Client::find($id);
        return response()->json($customer);
    }

    public function confirm($id)
    {
        $customer = Client::find($id);

        return response()->json($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $customer = $this->customer->apiUpdateCustomer($id, $request->all());
        session()->forget('custom');
        return response()->json($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->customer->destroy($id);

        return response()->json('successfully deleted');
    }
}
