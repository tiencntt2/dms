<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TantouResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "code" => $this->code,
            "login_id" => $this->login_id,
            "name" => $this->name,
            "email" => $this->email,
            "order" => $this->order,
            "role" => $this->role,
            'created_at' => Carbon::parse($this->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y/m/d'),
            "updated_user" => $this->updated_user,
            "deleted_flg" => $this->deleted_flg,
        ];
    }
}
