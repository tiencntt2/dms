<?php

namespace App\Http\Resources;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ImportLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);

        return [
            'ID' => $this->id,
            '実装時間' => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
            'ファイル名' => $this->file_name,
            '担当者' => ($this->user_id) ? (!empty($user) ? $user->name : '') : '',
            '納品書数' => $this->total_record,
            'ステータス' => $this->status,
        ];
    }
}
