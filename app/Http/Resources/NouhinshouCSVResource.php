<?php

namespace App\Http\Resources;

use App\Models\Client;
use App\Models\Nouhinshous;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class NouhinshouCSVResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $code1 = Client::select('code')->where('id', parent::toArray($request)['seikyusaki_id'])->first();
        $code2 = Client::select('code')->where('id', parent::toArray($request)['nonyusaki_id'])->first();
        $code3 = User::select('code')->where('id', parent::toArray($request)['tantou_id'])->first();
        $response = [
            'seikyusaki_id' => isset($code1->code) ? $code1->code : '',//index 0
            'seikyusaki_text' => parent::toArray($request)['seikyusaki_text'],//index 1
            'nonyusaki_id' => isset($code2->code) ? $code2->code : '',//index 2
            'nonyusaki_text' => parent::toArray($request)['nonyusaki_text'],//index 3
            'tantou_id' => isset($code3->code) ? $code3->code : '',//index 4
            'confirm' => '',//index 5
            'denpyo_number' => parent::toArray($request)['denpyo_number'],//index 6
            ];
        //index 7
        if (isset(parent::toArray($request)['nouhinshou_meisais'])) {
            $response['meisai_line'] = isset(parent::toArray($request)['nouhinshou_meisais']['meisai_line']) ? parent::toArray($request)['nouhinshou_meisais']['meisai_line'] : '';
        } else {
            $response['meisai_line'] = '';
        }

            $response['confirm2'] = '';//index 8
            $response['torihiki_code'] = parent::toArray($request)['torihiki_code'];//index 9
            $response['torihiki_text'] = parent::toArray($request)['torihiki_text'];//index 10
            $response['torihiki_datetime'] = parent::toArray($request)['torihiki_datetime'];//index 11
        if (isset(parent::toArray($request)['nouhinshou_meisais'])) {
            $response['item_name'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_name']) ? parent::toArray($request)['nouhinshou_meisais']['item_name'] : '';//index 12
            $response['item_class'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_class']) ? parent::toArray($request)['nouhinshou_meisais']['item_class'] : '';//index 13
            $response['tekiyo1'] = isset(parent::toArray($request)['nouhinshou_meisais']['tekiyo1']) ? parent::toArray($request)['nouhinshou_meisais']['tekiyo1'] : '';//index 14
            $response['confirm3'] = '';//index 15
            $response['item_code'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_code']) ? parent::toArray($request)['nouhinshou_meisais']['item_code'] : '';//index 16
            $response['item_number'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_number']) ? parent::toArray($request)['nouhinshou_meisais']['item_number'] : '';//index 17
            $response['confirm4'] = '';//index 18
            $response['item_quantity'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_quantity']) ? parent::toArray($request)['nouhinshou_meisais']['item_quantity'] : '';//index 19
            $response['item_tanka'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_tanka']) ? parent::toArray($request)['nouhinshou_meisais']['item_tanka'] : '';//index 20
            $response['kingaku'] = isset(parent::toArray($request)['nouhinshou_meisais']['kingaku']) ? parent::toArray($request)['nouhinshou_meisais']['kingaku'] : '';//index 21
            $response['zei'] = isset(parent::toArray($request)['nouhinshou_meisais']['zei']) ? parent::toArray($request)['nouhinshou_meisais']['zei'] : '';//index 22
            $response['tekiyo2'] = isset(parent::toArray($request)['nouhinshou_meisais']['tekiyo2']) ? parent::toArray($request)['nouhinshou_meisais']['tekiyo2'] : '';//index 23
        } else {
            $response['item_name'] = '';//index 12
            $response['item_class'] = '';//index 13
            $response['tekiyo1'] = '';//index 14
            $response['confirm3'] = '';//index 15
            $response['item_code'] = '';//index 16
            $response['item_number'] = '';//index 17
            $response['confirm4'] = '';//index 18
            $response['item_quantity'] = '';//index 19
            $response['item_tanka'] = '';//index 20
            $response['kingaku'] = '';//index 21
            $response['zei'] = '';//index 22
            $response['tekiyo2'] = '';//index 23
        }
        $response['biko_text'] = parent::toArray($request)['biko_text'];//index 24
        $response['confirm5'] = '';//index 25
        return $response;
    }
}
