<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NouhinshouMeisaisResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'item_code' => $this->item_code ?? null,
            'item_name' => $this->item_name ?? null,
            'item_kikaku' => $this->item_kikaku ?? null,
            'item_quantity' => $this->item_quantity ?? null,
            'item_tanka' => $this->item_tanka ?? null,
            'kingaku' => $this->kingaku ?? null,
            'tekiyo1' => $this->tekiyo1 ?? null,
            'tekiyo2' => $this->tekiyo2 ?? null,
            'zei' => $this->zei ?? null,
            'meisai_line' => $this->meisai_line ?? null,
        ];
    }
}
