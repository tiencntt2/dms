<?php

namespace App\Http\Resources;

use App\Models\Client;
use App\Models\NouhinshouMeisai;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NouhinshouDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $nonyusaki = Client::find($this->nonyusaki_id);
        $seikyusaki = Client::find($this->seikyusaki_id);
        $tantou = User::find($this->tantou_id);
        $nouhinshou_meisais = NouhinshouMeisai::where('nouhinshou_id', $this->id)
            ->where('deleted_flg', NouhinshouMeisai::NOT_DELETE)
            ->orderBy('meisai_line')
            ->get();

        return [
            'id' => $this->id,
            'denpyo_number' => $this->denpyo_number ?? null,
            'torihiki_text' => $this->torihiki_text ?? null,
            'torihiki_code' => $this->torihiki_code ?? null,
            'nonyusaki_id' => $this->nonyusaki_id ?? null,
            'nonyusaki_name' => !empty($nonyusaki) ? $nonyusaki->name : null,
            'seikyusaki_id' => $this->seikyusaki_id ?? null,
            'seikyusaki_name' => !empty($seikyusaki) ? $seikyusaki->name : null,
            'tantou_id' => $this->tantou_id ?? null,
            'tantou_name' => !empty($tantou) ? $tantou->name : null,
            'torihiki_datetime' => $this->torihiki_datetime ? Carbon::parse($this->torihiki_datetime)->format('Y/m/d') : null,
            'biko_text' => $this->biko_text ?? null,
            'kingaku' => $this->kingaku ?? null,
            'nouhinshou_meisais' => NouhinshouMeisaisResource::collection($nouhinshou_meisais),
            'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('Y/m/d') : null,
            'updated_at' => $this->updated_at ? Carbon::parse($this->updated_at)->format('Y/m/d') : null
        ];
    }
}
