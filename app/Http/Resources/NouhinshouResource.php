<?php

namespace App\Http\Resources;

use App\Models\Client;
use App\Models\NouhinshouMeisai;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NouhinshouResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $nonyusaki = Client::find($this->nonyusaki_id);
        $seikyusaki = Client::find($this->seikyusaki_id);
        $tantou = User::find($this->tantou_id);
        $meisais_combine = NouhinshouMeisai::where('nouhinshou_id',$this->id)
            ->where('meisai_line', NouhinshouMeisai::COMBINE)
            ->first();

        return [
            'id' => $this->id,
            'denpyo_number' => $this->denpyo_number ?? null,
            'torihiki_text' => $this->torihiki_text ?? null,
            'nonyusaki_name' => !empty($nonyusaki) ? $nonyusaki->name : null,
            'seikyusaki_name' => !empty($seikyusaki) ? $seikyusaki->name : null,
            'tantou_name' => !empty($tantou) ? $tantou->name : null,
            'torihiki_datetime' => $this->torihiki_datetime ? Carbon::parse($this->torihiki_datetime)->format('Y/m/d') : null,
            'biko_text' => $this->biko_text ?? null,
            'total_kingaku' => $meisais_combine ? $meisais_combine->kingaku : 0,
            'created_at' => $this->created_at ? Carbon::parse($this->created_at)->format('Y/m/d') : null,
            'updated_at' => $this->updated_at ? Carbon::parse($this->updated_at)->format('Y/m/d') : null
        ];
    }
}
