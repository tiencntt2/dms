<?php

namespace App\Http\Resources;

use App\Models\Client;
use App\Models\Nouhinshous;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class NouhinshouCSVResource2 extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $code1 = Client::select('code')->where('id', parent::toArray($request)['seikyusaki_id'])->first();
        $code2 = Client::select('code')->where('id', parent::toArray($request)['nonyusaki_id'])->first();
        $code3 = User::select('code')->where('id', parent::toArray($request)['tantou_id'])->first();
        $response = [
            'seikyusaki_id' => isset($code1->code) ? $code1->code : '',//index 0
            'seikyusaki_text' => parent::toArray($request)['seikyusaki_text'],//index 1
            'nonyusaki_id' => isset($code2->code) ? $code2->code : '',//index 2
            'nonyusaki_text' => parent::toArray($request)['nonyusaki_text'],//index 3
            'tantou_id' => isset($code3->code) ? $code3->code : '',//index 4
            'torihiki_code' => parent::toArray($request)['torihiki_code'],//index 5
            'torihiki_text' => parent::toArray($request)['torihiki_text'],//index 6
            'torihiki_datetime' => parent::toArray($request)['torihiki_datetime'],//index 7
            'denpyo_number' => parent::toArray($request)['denpyo_number'],//index 8
            ];
        if (isset(parent::toArray($request)['nouhinshou_meisais'])) {
            $response['meisai_line'] = isset(parent::toArray($request)['nouhinshou_meisais']['meisai_line']) ? parent::toArray($request)['nouhinshou_meisais']['meisai_line'] : '';//index 9
            $response['item_code'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_code']) ? parent::toArray($request)['nouhinshou_meisais']['item_code'] : '';//index 10
            $response['item_name'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_name']) ? parent::toArray($request)['nouhinshou_meisais']['item_name'] : '';//index 11
            $response['item_kikaku'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_kikaku']) ? parent::toArray($request)['nouhinshou_meisais']['item_kikaku'] : '';//index 12
            $response['item_quantity'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_quantity']) ? parent::toArray($request)['nouhinshou_meisais']['item_quantity'] : '';//index 13
            $response['item_tanka'] = isset(parent::toArray($request)['nouhinshou_meisais']['item_tanka']) ? parent::toArray($request)['nouhinshou_meisais']['item_tanka'] : '';//index 14
            $response['kingaku'] = isset(parent::toArray($request)['nouhinshou_meisais']['kingaku']) ? parent::toArray($request)['nouhinshou_meisais']['kingaku'] : '';//index 15
            $response['zei'] = isset(parent::toArray($request)['nouhinshou_meisais']['zei']) ? parent::toArray($request)['nouhinshou_meisais']['zei'] : '';//index 16
            $response['tekiyo1'] = isset(parent::toArray($request)['nouhinshou_meisais']['tekiyo1']) ? parent::toArray($request)['nouhinshou_meisais']['tekiyo1'] : '';//index 17
            $response['tekiyo2'] = isset(parent::toArray($request)['nouhinshou_meisais']['tekiyo2']) ? parent::toArray($request)['nouhinshou_meisais']['tekiyo2'] : '';//index 18
        } else {
            $response['meisai_line'] = '';//index 9
            $response['item_code'] = '';//index 10
            $response['item_name'] = '';//index 11
            $response['item_kikaku'] = '';//index 12
            $response['item_quantity'] = '';//index 13
            $response['item_tanka'] = '';//index 14
            $response['kingaku'] = '';//index 15
            $response['zei'] = '';//index 16
            $response['tekiyo1'] = '';//index 17
            $response['tekiyo2'] = '';//index 18
        }
        $response['biko_text'] = parent::toArray($request)['biko_text'];//index 19
        return $response;
    }
}
