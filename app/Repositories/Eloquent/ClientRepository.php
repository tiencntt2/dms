<?php

namespace App\Repositories\Eloquent;

use App\Models\Client;
use App\Repositories\EloquentRepository;

class ClientRepository extends EloquentRepository
{
    public function model()
    {
        return Client::class;
    }

    public function apiPaginateBy(
        $query = [],
        $orderBy = 'id',
        $order = 'asc',
        $limit = 15,
        $columns = ['clients.*']
    ) {
        $builder = $this->model->orderBy($orderBy ?: 'id', $order ?: 'asc');

        if (count($query)) {
            foreach ($query as $field => $value) {
                if ($value) {
                    $builder->where($field, '=', $value);
                }
            }
        }

        $builder->where('deleted_flg', '=', Client::NOT_DELETE);

        return $builder->paginate($limit, $columns);
    }



    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            if ($id instanceof $this->model) {
                $obj = $id;
            } else {
                $obj = $this->find($id);
            }

            $obj->deleted_flg = Client::DELETETED;
            $response =$obj->save();

            \DB::commit();

            return $response;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return false;
        }
    }
}
