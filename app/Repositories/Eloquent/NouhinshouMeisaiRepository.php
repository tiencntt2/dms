<?php

namespace App\Repositories\Eloquent;

use App\Models\NouhinshouMeisai;
use App\Repositories\EloquentRepository;

class NouhinshouMeisaiRepository extends EloquentRepository
{
    public function model()
    {
        return NouhinshouMeisai::class;
    }


    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            if ($id instanceof $this->model) {
                $obj = $id;
            } else {
                $obj = $this->find($id);
            }

            $obj->deleted_flg = NouhinshouMeisai::DELETETED;
            $response =$obj->save();

            \DB::commit();

            return $response;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return false;
        }
    }

    public function updateNouhinshouMeisaiCombine($nouhinshouId)
    {
        $nouhinshouMeisais = NouhinshouMeisai::where('nouhinshou_id', $nouhinshouId)
            ->where('meisai_line', '!=', NouhinshouMeisai::COMBINE)
            ->where('deleted_flg', NouhinshouMeisai::NOT_DELETE)
            ->get();

        $nouhinshouMeisaiCombine = NouhinshouMeisai::where('nouhinshou_id', $nouhinshouId)
            ->where('meisai_line', NouhinshouMeisai::COMBINE);

        $meisaiCombine = $nouhinshouMeisaiCombine->get();

        if (count($nouhinshouMeisais) == 0 && $meisaiCombine) {
            $nouhinshouMeisaiCombine->delete();
        } else {
            $meisaiCombine = $meisaiCombine->last();
            $rate = $meisaiCombine->zei / $meisaiCombine->kingaku;
            $item_quantity = $nouhinshouMeisais->sum('item_quantity');
            $kingaku = $nouhinshouMeisais->sum('kingaku');
            $zei = $kingaku * $rate;
            $meisaiCombine->update(['item_quantity' => $item_quantity, 'kingaku' => $kingaku, 'zei' => $zei]);
        }
    }
}
