<?php

namespace App\Repositories\Eloquent;

use App\Models\ImportLog;
use App\Repositories\EloquentRepository;

class ImportLogRepository extends EloquentRepository
{
    public function model()
    {
        return ImportLog::class;
    }

    public function apiPaginateBy(
        $orderBy = 'id',
        $order = 'asc',
        $limit = 10,
        $columns = ['import_logs.*']
    ) {
        $builder = $this->model->orderBy($orderBy ?: 'id', $order ?: 'asc');

        return $builder->paginate($limit, $columns);
    }
}
