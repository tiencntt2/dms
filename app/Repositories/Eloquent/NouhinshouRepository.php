<?php

namespace App\Repositories\Eloquent;

use App\Models\Client;
use App\Models\Nouhinshous;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\Log;

class NouhinshouRepository extends EloquentRepository
{
    public function model()
    {
        return Nouhinshous::class;
    }

    public function apiPaginateBy(
        $role = 'admin',
        $client_id = '',
        $client_type = '',
        $torihiki_datetime = [],
        $query = [],
        $orderBy = 'id',
        $order = 'asc',
        $limit = 50,
        $columns = ['nouhinshous.*']

    ) {
        $builder = $this->model->orderBy($orderBy ?: 'id', $order ?: 'asc');

        if (count($torihiki_datetime)) {
            $builder->where(function ($builder) use ($torihiki_datetime){
                if ($torihiki_datetime['date_from']) {
                    $builder->where('torihiki_datetime', '>=', $torihiki_datetime['date_from']);
                }
                if ($torihiki_datetime['date_to']) {
                    $builder->where('torihiki_datetime', '<=', $torihiki_datetime['date_to']);
                }
            });
        }

        if (isset($role) && $role) {
            $builder->where(function ($builder) use ($role, $client_id, $client_type) {
                if ($role == 'admin') {
                    $builder->whereNotNull('tantou_id');
                } else {
                    if ($client_id) {
                        if ($client_type == Client::TYPE_SEIKYUSAKI) {
                            $builder->where('seikyusaki_id', $client_id);
                        } else {
                            $builder->where('nonyusaki_id', $client_id);
                        }
                    }
                }
            });
        }

        if (count($query)) {
            $builder->where(function ($builder) use ($query) {
                foreach ($query as $field => $value) {
                    if (strlen($value)) {
                        $operator = '=';
                        if (strstr($field, 'text')) {
                            $operator = 'like';
                            $value = '%' . $value . '%';
                        }
                        $builder->where($field, $operator, $value);
                    }
                }
            });
        }
        $builder->where(function ($builder) {
            $builder->where('deleted_flg', '=', Nouhinshous::NOT_DELETE);
        });

        return $builder->paginate($limit, $columns);
    }

    public function listCSVs(
        $client_id = '',
        $torihiki_datetime = [],
        $query = []
    ) {
        $builder = $this->model->select('id');

        if (count($torihiki_datetime)) {
            $builder->where(function ($builder) use ($torihiki_datetime){
                if ($torihiki_datetime['date_from']) {
                    $builder->where('torihiki_datetime', '>=', $torihiki_datetime['date_from']);
                }
                if ($torihiki_datetime['date_to']) {
                    $builder->where('torihiki_datetime', '<=', $torihiki_datetime['date_to']);
                }
            });
        }

        if (isset($query['denpyo_number']) && $query['denpyo_number']) {
            $builder->where('denpyo_number', 'like', '%'.$query['denpyo_number'].'%');
        }
        if (isset($query['torihiki_text']) && $query['torihiki_text']) {
            $builder->where('torihiki_text', 'like', '%'.$query['torihiki_text'].'%');
        }
        if (isset($query['biko_text']) && $query['biko_text']) {
            $builder->where('biko_text', 'like', '%'.$query['biko_text'].'%');
        }

        if ($client_id) {
            $builder->where(function ($query) use($client_id) {
                $query->where('nonyusaki_id', $client_id)
                    ->orWhere('seikyusaki_id', $client_id);
            });
        }

        $builder->where(function ($builder) {
            $builder->where('deleted_flg', '=', Nouhinshous::NOT_DELETE);
        });

        return $builder->get();
    }

    public function listNouhinshouCSVBy($array_id) {
        $builder = $this->model->with('nouhinshouMeisais')->whereIn('id', $array_id);
        $builder->where(function ($builder) {
            $builder->where('deleted_flg', '=', Nouhinshous::NOT_DELETE);
        });

         return $builder->get();
    }

    public function getNouhinshou($id){
        return $this->model->with('nouhinshouMeisais')->where('id', $id)->first();
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            if ($id instanceof $this->model) {
                $obj = $id;
            } else {
                $obj = $this->find($id);
            }

            $obj->deleted_flg = Nouhinshous::DELETETED;
            $response =$obj->save();

            \DB::commit();

            return $response;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return false;
        }
    }
}
