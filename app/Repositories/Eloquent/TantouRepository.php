<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\EloquentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class TantouRepository extends EloquentRepository
{
    public function model()
    {
        return User::class;
    }

    public function apiListTantou($param) {
        $builder = $this->model->where('deleted_flg', '<>', 1)->orderBy('id');
        if (isset($param['name']) && $param['name']) {
            $builder->where('name', 'like', '%'.$param['name'].'%');
        }
        if (isset($param['login_id']) && $param['login_id']) {
            $builder->where('login_id', 'like', '%'.$param['login_id'].'%');
        }
        return $builder->paginate($param['limit']);
    }

    public function apiStoreTantou($param) {
        \DB::beginTransaction();
        try {
            $attribute = [
                'code' => isset($param['code']) ? $param['code'] : '',
                'name' => isset($param['name']) ? $param['name'] : '',
                'email' => isset($param['email']) ? $param['email'] : '',
                'login_id' => isset($param['login_id']) ? $param['login_id'] : '',
                'password' => isset($param['password']) ? Hash::make($param['password']) : '',
                'created_at' => Carbon::now()->format('Ymdhis'),
                'updated_at' => Carbon::now()->format('Ymdhis'),
            ];
            $tantou = $this->create($attribute);
            $role = [
                'role_id' => User::MODEL_HAS_ROLE,
                'model_type' => 'App\Models\User',
                'model_id' => $tantou->id,
            ];
            \DB::table('model_has_roles')->insert($role);
            \DB::commit();
            return $tantou;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }
    }

    public function apiUpdateTantou($id, $param) {
        $tantou = $this->find($id);
        $attribute = [
            'code' => isset($param['code']) ? $param['code'] : '',
            'name' => isset($param['name']) ? $param['name'] : '',
            'email' => isset($param['email']) ? $param['email'] : '',
            'login_id' => isset($param['login_id']) ? $param['login_id'] : '',
            'password' => isset($param['password']) ?: $tantou->password,
            'updated_at' => Carbon::now()->format('Ymdhis'),
        ];
        if (isset($param['password']) && !empty($param['password']) && $param['password'] != User::PASSWORD_DEFAULT) {
            $attribute['password'] = isset($param['password']) ? Hash::make($param['password']) : '';
        }
        $tantou->update($attribute);
        return $tantou;
    }

    public function findLoginID($login_id) {
        return User::where('login_id', $login_id)->first();
    }

    public function findEmail($email) {
        return User::where('email', $email)->first();
    }

    public function findWithLoginIDAndEmail($login_id, $email) {
        return User::where('login_id', $login_id)
            ->where('email', $email)
            ->first();
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            $obj = $this->find($id);
            $obj->deleted_flg = User::DELETED;
            $response =$obj->save();

            \DB::commit();

            return $response;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return false;
        }
    }
}
