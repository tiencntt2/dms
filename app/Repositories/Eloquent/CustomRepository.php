<?php

namespace App\Repositories\Eloquent;

use App\Models\Client;
use App\Repositories\EloquentRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class CustomRepository extends EloquentRepository
{
    public function model()
    {
        return Client::class;
    }

    public function apiListCustomer($param) {
        $builder = $this->model->where('deleted_flg', '<>', 1)->orderBy('id');
        if (isset($param['name']) && $param['name']) {
            $builder->where('name', 'like', '%'.$param['name'].'%');
        }
        if (isset($param['code']) && $param['code']) {
            $builder->where('code', 'like', '%'.$param['code'].'%');
        }
        if (isset($param['login_id']) && $param['login_id']) {
            $builder->where(function ($query) use($param) {
                $query->where('login_id', 'like', '%'.$param['login_id'].'%')
                    ->orWhere('login_id2', 'like', '%'.$param['login_id'].'%');
            });
        }
        return $builder->paginate($param['limit']);
    }

    public function apiStoreCustomer($param) {
        \DB::beginTransaction();
        try {
            $attribute = [
                'type' => isset($param['type']) ? $param['type'] : '',
                'code' => isset($param['code']) ? $param['code'] : '',
                'name' => isset($param['name']) ? $param['name'] : '',
                'email' => isset($param['email']) ? $param['email'] : '',
                'postal_code' => isset($param['postal_code']) ? $param['postal_code'] : '',
                'address' => isset($param['address']) ? $param['address'] : '',
                'login_id' => isset($param['login_id']) ? $param['login_id'] : '',
                'password' => isset($param['password']) ? Hash::make($param['password']) : '',
                'login_id2' => isset($param['login_id2']) ? $param['login_id2'] : '',
                'password2' => isset($param['password2']) ? Hash::make($param['password2']) : '',
                'created_at' => Carbon::now()->format('Ymdhis'),
                'updated_at' => Carbon::now()->format('Ymdhis'),
            ];
            $customer = $this->create($attribute);
            $role = [
                'role_id' => Client::MODEL_HAS_ROLE,
                'model_type' => 'App\Models\Client',
                'model_id' => $customer->id,
            ];
            \DB::table('model_has_roles')->insert($role);
            \DB::commit();
            return $customer;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }
    }

    public function findLoginID($login_id) {
        return Client::where('login_id', $login_id)->first();
    }

    public function findEmail($email) {
        return Client::where('email', $email)->first();
    }

    public function findWithLoginIDAndEmail($login_id, $email) {
        return Client::where('login_id', $login_id)
            ->where('email', $email)
            ->first();
    }

    public function apiUpdateCustomer($id, $param) {
        $customer = $this->find($id);
        $attribute = [
            'type' => isset($param['type']) ? $param['type'] : '',
            'code' => isset($param['code']) ? $param['code'] : '',
            'name' => isset($param['name']) ? $param['name'] : '',
            'email' => isset($param['email']) ? $param['email'] : '',
            'postal_code' => isset($param['postal_code']) ? $param['postal_code'] : '',
            'address' => isset($param['address']) ? $param['address'] : '',
            'login_id' => isset($param['login_id']) ? $param['login_id'] : '',
            'password' => isset($param['password']) ? $customer->password : '',
            'login_id2' => isset($param['login_id2']) ? $param['login_id2'] : '',
            'password2' => isset($param['password2']) ? $customer->password2 : '',
            'updated_at' => Carbon::now()->format('Ymdhis'),
        ];
        if (isset($param['password']) && !empty($param['password']) && $param['password'] != Client::PASSWORD_DEFAULT) {
            $attribute['password'] = isset($param['password']) ? Hash::make($param['password']) : '';
        }

        if (isset($param['password2']) && !empty($param['password2']) && $param['password2'] != Client::PASSWORD_DEFAULT) {
            $attribute['password2'] = isset($param['password2']) ? Hash::make($param['password2']) : '';
        }
        $customer->update($attribute);
        return $customer;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        try {
            $obj = $this->find($id);
            $obj->deleted_flg = Client::DELETED;
            $response =$obj->save();

            \DB::commit();
            return $response;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);

            return false;
        }
    }
}
